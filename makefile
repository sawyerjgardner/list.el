MAKEFLAGS      += --output-sync=target

BUILD_DIR      := build
TEST_DIR       := tests
LOG_DIR        := ${BUILD_DIR}/logs

BYTECODE_DIR   := ${BUILD_DIR}/bytecode
TEST_BYTE_DIR  := ${BUILD_DIR}/bytecode_tests
LOG_BYTE_DIR   := ${BUILD_DIR}/bytecode_logs

TIMEOUT_FLAGS  := -s SIGUSR2 -k 10s 20s
EMACS_FLAGS    := -batch \
                  -l ert \
                  -eval "(setf ert-batch-backtrace-right-margin 120   \
                               ert-batch-print-length           20    \
                               ert-batch-print-level            20    \
                               print-length                     20    \
                               eval-expression-print-length     20    \
                               print-level                      20    \
                               eval-expression-print-level      20 )" \
                  -eval "(advice-add                                  \
                           'ert--pp-with-indentation-and-newline      \
                           :around                                    \
                           (lambda(oldfn &rest args)                  \
                             (let ((print-length 20)                  \
                                   (print-level  20) )                \
	                           (funcall oldfn args) )))"


### files

LISP_FILES_VLIST := vlist-tmp.el
LISP_FILES_LIST  := list.el $(shell find 'list' -name '*.el') list-tools.el
LISP_FILES       := $(LISP_FILES_LIST) $(LISP_FILES_VLIST)
LISP_FILES_ALL   := $(LISP_FILES)

LISP_TESTS_VLIST := $(TEST_DIR)/test-vlist-tmp.el
LISP_TESTS_LIST  := $(shell find '$(TEST_DIR)/list' -name '*.el')
LISP_TEST_UTILS  := $(TEST_DIR)/+test.el $(TEST_DIR)/split-template.el
LISP_TESTS       := $(LISP_TESTS_LIST) $(LISP_TESTS_VLIST)
LISP_TESTS_ALL   := $(LISP_TESTS) $(LISP_TEST_UTILS)

LISP_ALL         := $(LISP_TESTS_ALL) $(LISP_FILES_ALL)

define eq
$(and $(findstring x$1,x$2), $(findstring x$2,x$1))
endef
define assert_diff
	$(if $(call eq,$1,$2),$(error failed to change: "$1" in $3),$1)
endef
define assert_patsubst
	$(call assert_diff,$(patsubst $1,$2,$3),$3,$4)
endef

## compiled files

define lisp_file_to_compile
	$(call assert_patsubst,%.el,${BYTECODE_DIR}/%.elc,$1,lisp_file_to_compile)
endef
define lisp_compile_to_file
	$(call assert_patsubst,${BYTECODE_DIR}/%.elc,%.el,$1,lisp_compile_to_file)
endef

define lisp_test_to_tcompile
	$(call assert_patsubst,${TEST_DIR}/%.el,${TEST_BYTE_DIR}/%.elc,$1,lisp_test_to_tcompile)
endef
define lisp_tcompile_to_test
	$(call assert_patsubst,${TEST_BYTE_DIR}/%.elc,${TEST_DIR}/%.el,$1,lisp_tcompile_to_test)
endef

LISP_FILES_VLIST_COMILE := $(call lisp_file_to_compile,$(LISP_FILES_VLIST))
LISP_FILES_LIST_COMILE  := $(call lisp_file_to_compile,$(LISP_FILES_LIST))
LISP_FILES_COMILE       := $(call lisp_file_to_compile,$(LISP_FILES))
LISP_FILES_ALL_COMILE   := $(call lisp_file_to_compile,$(LISP_FILES_ALL))

LISP_TESTS_VLIST_COMILE := $(call lisp_test_to_tcompile,$(LISP_TESTS_VLIST))
LISP_TESTS_LIST_COMILE  := $(call lisp_test_to_tcompile,$(LISP_TESTS_LIST))
LISP_TEST_UTILS_COMILE  := $(call lisp_test_to_tcompile,$(LISP_TEST_UTILS))
LISP_TESTS_COMILE       := $(call lisp_test_to_tcompile,$(LISP_TESTS))
LISP_TESTS_ALL_COMILE   := $(call lisp_test_to_tcompile,$(LISP_TESTS_ALL))

LISP_ALL_COMILE         := $(LISP_FILES_COMILE) $(LISP_TESTS_ALL_COMILE)

## test logs

define lisp_test_to_log
	$(call assert_patsubst,$(TEST_DIR)/%.el,${LOG_DIR}/%.log,$1,lisp_test_to_log)
endef
define lisp_log_to_test
	$(call assert_patsubst,${LOG_DIR}/%.log,$(TEST_DIR)/%.el,$1,lisp_log_to_test)
endef

define lisp_tcompile_to_log_byte
	$(call assert_patsubst,$(TEST_BYTE_DIR)/%.elc,${LOG_BYTE_DIR}/%.log,$1,lisp_tcompile_to_log_byte)
endef
define lisp_log_byte_to_tcompile
	$(call assert_patsubst,${LOG_BYTE_DIR}/%.log,$(TEST_BYTE_DIR)/%.elc,$1,lisp_log_byte_to_tcompile)
endef

LOG_VLIST         := $(call lisp_test_to_log,$(LISP_TESTS_VLIST))
LOG_LIST          := $(call lisp_test_to_log,$(LISP_TESTS_LIST))
LOG_ALL           := $(call lisp_test_to_log,$(LISP_TESTS))

LOG_VLIST_COMPILE := $(call lisp_tcompile_to_log_byte,$(LISP_TESTS_VLIST_COMILE))
LOG_LIST_COMPILE  := $(call lisp_tcompile_to_log_byte,$(LISP_TESTS_LIST_COMILE))
LOG_ALL_COMPILE   := $(call lisp_tcompile_to_log_byte,$(LISP_TESTS_COMILE))

LISP_ALL_LOG      := $(LOG_ALL) $(LOG_ALL_COMPILE)


### functions

#(call test_fun,"text",command,log_to)
define print_if_error
	@OUTPUT=$$(timeout $(TIMEOUT_FLAGS) $2 2>&1) ; ERR=$$?; \
	if [ $${ERR} -eq 0 ] ; then                             \
        printf "%s: %-50s - [OK]\n" $1 $<;                  \
		$(if $3,echo "$${OUTPUT}" > $3;)                    \
	else                                                    \
        printf "%s: %-50s - [ERROR]\n" $1 $<;               \
		echo "$${OUTPUT}" $(if $3,| tee $3,; exit $${ERR}); \
	fi
endef


#(call run_build_inplace,flags,file)
define run_build_inplace
	$(call print_if_error,"building",                     \
		emacs $(EMACS_FLAGS) $1 -f batch-byte-compile $2)
endef

#(call run_build_tmp,flags,from,to-fake)
define run_build_tmp
	@cp $2 $3
	$(call run_build_inplace, $1 -eval "(setq byte-compile-error-on-warn t)",$3)
	@rm $3
endef

#(call run_build,flags,from,to)
define run_build
	$(call run_build_tmp,$1,$2,$(addsuffix .el,$(basename $3)))
endef

#(call run_tests,flags)
define run_tests
	$(call print_if_error,"testing",                                  \
		emacs $(EMACS_FLAGS) $1 -f ert-run-tests-batch-and-exit 2>&1, \
		$@)
endef

#(call run_summery,log_files)
define run_summery
	 @emacs $(EMACS_FLAGS) -f ert-summarize-tests-batch-and-exit $1
endef


### targets

.SECONDEXPANSION:
$(LISP_FILES_ALL_COMILE):%:$$(call lisp_compile_to_file,%)
	@mkdir -p $(@D)
	$(call run_build, -L "$(PWD)",$^,$@)

.SECONDEXPANSION:
$(LISP_TESTS_ALL_COMILE):%:$$(call lisp_tcompile_to_test,%)
	@mkdir -p $(@D)
	$(call run_build, -L "$(PWD)/${TEST_DIR}" -L "$(PWD)",$^,$@)


.SECONDEXPANSION:
$(LOG_ALL):%:$$(call lisp_log_to_test,%)
	@mkdir -p $(@D)
	$(call run_tests, -L "$(PWD)/${TEST_DIR}" -L "$(PWD)" $(addprefix -l ,$^))

.SECONDEXPANSION:
$(LOG_ALL_COMPILE):%:$$(call lisp_log_byte_to_tcompile,%) $$(LISP_ALL_COMILE)
	@mkdir -p $(@D)
	$(call run_tests, -L "$(PWD)/${TEST_BYTE_DIR}" -L "$(PWD)/${BYTECODE_DIR}" $(addprefix -l ,$<))


.DEFAULT_GOAL:
build: $(LISP_ALL_COMILE)

test_vlist: $(LOG_VLIST)
	$(call run_summery,$^)
test_list: $(LOG_LIST)
	$(call run_summery,$^)
test: $(LOG_ALL)
	$(call run_summery,$^)

test_byte_vlist: $(LOG_VLIST_COMPILE)
	$(call run_summery,$^)
test_byte_list: $(LOG_LIST_COMPILE)
	$(call run_summery,$^)
test_byte: $(LOG_ALL_COMPILE)
	$(call run_summery,$^)

test_all_vlist: $(LOG_VLIST) $(LOG_VLIST_COMPILE)
	$(call run_summery,$^)
test_all_list: $(LOG_LIST) $(LOG_LIST_COMPILE)
	$(call run_summery,$^)
test_all: $(LISP_ALL_LOG)
	$(call run_summery,$^)
