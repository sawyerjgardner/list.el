;;; vlist-tmp.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: May 26, 2022
;; Modified: May 26, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/vlist-tmp
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'list)

(defun vlist-drop-until-after(_list spot)
  ""
  (list-copy (cdr spot)) )

(gv-define-expander vlist-drop-until-after
  (lambda(do sub-place spot)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* ignore ((at  spot)
                              (end `(cdr ,at)) )
        (funcall do `(list-copy end)
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        (if (list-length=* ,v ,end)
                            (list-copy-to ,v ,end)
                          (setf (cdr ,at) nil)
                          (list-concat-place ,sub-got ,v)
                          (setf (cdr ,at) ,end) )
                        ,v ))))))))


(defun vlist-drop(list n)
  "Return a shallow copy of LIST without the first N elements.
if N is zero or a negative number, LIST is returned not copied.
this function is desighned to treat the list LIST like a vector
to be consistent with `seq-drop'.

when a value VAL is being asighned to this place:
if N is zero or a nagitive number, VAL is asighned to the place LIST.
if VAL is a list the length of LIST minus N, the elements of VAL are copyd to
the end of LIST.
otherwise a new list is created containing a shalow copy of the first n elements
of LIST and the elements of VAL."
  (if (> n 0)
      (list-copy (list-ndrop list n) )
    list ))

;;TODO: optimize?
(gv-define-expander vlist-drop
  (lambda(do sub-spot n)
    (list-tools--gv-letplace-simple sub-got sub-spot
      (macroexp-let2 macroexp-copyable-p idx n
        (funcall do nil ;;FIXME: should not be nil
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        (cond ((< ,idx 0)
                               (signal 'args-out-of-range (list ,sub-got ,idx)) )
                              ((= ,idx 0)
                               (setf ,sub-got ,v) )
                              ((= (list-length ,sub-got) (+ (list-length ,v) ,idx))
                               (list-copy-to ,v (list-ndrop ,sub-got ,idx)) )
                              (t
                               (setf (list-drop ,sub-got ,idx) ,v) ))
                        ,v ))))))))


(defun vlist-drop-until-cdr(list pred)
  "Return a shallow copy of LIST without the first N elements.
if N is zero or a negative number, LIST is returned not copied."
  (let ((out (list-ndrop-until-cdr list pred)))
    (if (eq out list)
        list
      (list-copy out) )))

;;TODO: optimize?
;;FIXME: calls pred too many times
(gv-define-expander vlist-drop-until-cdr
  (lambda(do sub-spot pred)
    (list-tools--gv-letplace-simple sub-got sub-spot
      (macroexp-let2 macroexp-copyable-p fn pred
        (funcall do nil ;;FIXME: should not be nil
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        (cond ((or (not ,sub-got)
                                   (funcall ,fn ,sub-got) )
                               (setf ,sub-got ,v) )
                              ((= (list-length ,v) (list-length (list-ndrop-until-cdr ,sub-got ,fn)))
                               (list-copy-to ,v (list-ndrop-until-cdr ,sub-got ,fn)) )
                              (t
                               (setf (list-drop-until-cdr ,sub-got ,fn) ,v) ))
                        ,v ))))))))


(defun vlist-drop-until(list pred)
  "
if N is zero or a negative number, LIST is returned not copied."
  (vlist-drop-until-cdr list (lambda(x) (funcall pred (car x)))) )

(gv-define-expander vlist-drop-until
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x)))) )
          (gv-get `(vlist-drop-until-cdr ,sub-got ,func) do) )))))


(defun vlist-drop-until-elt(list elt &optional testfn)
  "
if N is zero or a negative number, LIST is returned not copied."
  (setf testfn (or testfn #'equal))
  (vlist-drop-until-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(gv-define-expander vlist-drop-until-elt
  (lambda(do sub-place elt &optional testfn)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* macroexp-copyable-p ((e elt)
                                           (fn `(or ,testfn #'equal)))
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x) ,e))) )
          (gv-get `(vlist-drop-until-cdr ,sub-got ,func) do) )))))


;;split

(defun vlist-split-when-after(list spot)
  ""
  (if-let ((end (cdr spot)))
      (prog2
          (setf (cdr spot) nil)
          (list (list-copy list) (list-copy end))
        (setf (cdr spot) end) )
    (list list nil) ))

(defun vlist-split(list index)
  ""
  (if (> index 0)
      (vlist-split-when-after list (list-ndrop list (1- index)))
    (list nil list) ))

(defun vlist-split-when-cdr(list pred)
  ""
  (if (and list
           (not (funcall pred list)) )
      (vlist-split-when-after list (list--last-before-cdr1-skip list pred))
    (list nil list) ))

(defun vlist-split-when(list pred)
  ""
  (vlist-split-when-cdr list (lambda(x) (funcall pred (car x)))) )

(defun vlist-split-when-elt(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (vlist-split-when-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(defun vlist-partition(list index)
  ""
  (when (and list
             (> index 0) )
    (let ((split (vlist-split list index)))
      (setf (cdr split) (list-npartition (cadr split) index))
      split )))

;;TODO: this could probably be done better
(defun vlist-partition-when-cdr(list pred)
  ""
  (when list
    (let ((out (when (funcall pred list)
                 (list nil) )))
      (let ((split (list-split-when-after* list (list--last-before-cdr1-skip list pred))))
        (setf list            (cadr split)
              (list-tail out) (cons (car split) nil) ))
      (list-extend-while (out list)
        (let ((split (list-split-when-after* list (list--last-before-cdr1-skip list pred))))
          (setf list (cadr split))
          (if list
              (car split)
            (list-copy (car split)) ))))))

(defun vlist-partition-when(list pred)
  ""
  (vlist-partition-when-cdr list (lambda(x) (funcall pred (car x)))) )

(defun vlist-partition-when-elt(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (vlist-partition-when-cdr list (lambda(x) (funcall testfn (car x) elt))) )

;;take

(defun vlist-take-until-after(list spot)
  ""
  (if-let ((end (cdr spot)))
      (prog2
          (setf (cdr spot) nil)
          (list-copy list)
        (setf (cdr spot) end) )
    list ))

(gv-define-expander vlist-take-until-after
  (lambda(do sub-place spot)
    (gv-letplace (sub-getter sub-setter) sub-place
      (macroexp-let2* ignore ((sub-got sub-getter)
                              (split   `(list-split-when-after* ,sub-got ,spot)))
        (funcall do `(if (cadr ,split)
                         (car ,split)
                       ,sub-got )
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        ,(funcall sub-setter
                                  `(cond ((null (cadr ,split))
                                          ,v )
                                         ((list-length=* ,v (car ,split))
                                          (list-copy-to ,v ,sub-got)
                                          ,sub-got )
                                         (t
                                          (list-concat ,v (cadr ,split)) )))
                        ,v ))))))))

(defun vlist-take(list n)
  "Return a shallow copy of the first N elements of LIST.
if N is greater then the length of LIST, LIST is returned not copied.
this function is desighned to treat the list LIST like a vector
to be consistent with `seq-drop'.

when a value VAL is being asighned to this place:
if N is zero or a nagitive number, VAL is asighned to the place LIST.
if VAL is a list the length of LIST minus N, the elements of VAL are copyd to
the end of LIST.
otherwise a new list is created containing a shalow copy of the first n elements
of LIST and the elements of VAL."
  (when (> n 0)
    (vlist-take-until-after list (list-ndrop list (1- n))) ))

;;TODO: optimize?
(gv-define-expander vlist-take
  (lambda(do sub-spot n)
    (list-tools--gv-letplace-simple sub-got sub-spot
      (macroexp-let2* macroexp-copyable-p ((got-length `(list-length ,sub-got))
                                           (idx n)
                                           (r-idx `(- ,got-length ,idx)))
        (funcall do nil ;;FIXME: should not be nil
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        (cond ((< ,r-idx 0)
                               (signal 'args-out-of-range (list ,sub-got ,r-idx)) )
                              ((= ,r-idx 0)
                               (setf ,sub-got ,v) )
                              ((= (list-length ,sub-got) (+ (list-length ,v) ,r-idx))
                               (list-copy-to ,v ,sub-got) )
                              (t
                               (setf (list-take ,sub-got ,idx) ,v) ))
                        ,v ))))))))

(defun vlist-take-until-cdr(list pred)
  "Return a shallow copy of LIST without the first N elements.
if N is zero or a negative number, LIST is returned not copied."
  (when (and list
             (not (funcall pred list)) )
    (vlist-take-until-after list (list--last-before-cdr1-skip list pred)) ))

(gv-define-expander vlist-take-until-cdr
  (lambda(do sub-place pred)
    (gv-letplace (sub-getter sub-setter) sub-place
      (macroexp-let2* ignore ((sub-got sub-getter)
                              (split   `(list-split-when-cdr* ,sub-got ,pred)))
        (funcall do `(if (cadr ,split)
                         (car ,split)
                       ,sub-got )
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        ,(funcall sub-setter
                                  `(cond ((null (cadr ,split))
                                          ,v )
                                         ((list-length=* ,v (car ,split))
                                          (list-copy-to ,v ,sub-got)
                                          ,sub-got )
                                         (t
                                          (list-concat ,v (cadr ,split)) )))
                        ,v ))))))))

(defun vlist-take-until(list pred)
  "
if N is zero or a negative number, LIST is returned not copied."
  (vlist-take-until-cdr list (lambda(x) (funcall pred (car x)))) )

(gv-define-expander vlist-take-until
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x)))) )
          (gv-get `(vlist-take-until-cdr ,sub-got ,func) do) )))))

(defun vlist-take-until-elt(list elt &optional testfn)
  "
if N is zero or a negative number, LIST is returned not copied."
  (setf testfn (or testfn #'equal))
  (vlist-take-until-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(gv-define-expander vlist-take-until-elt
  (lambda(do sub-place elt &optional testfn)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* macroexp-copyable-p ((e elt)
                                           (fn `(or ,testfn #'equal)))
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x) ,e))) )
          (gv-get `(vlist-take-until-cdr ,sub-got ,func) do) )))))



(provide 'vlist-tmp)
;; (provide 'vlist)
;;; vlist-tmp.el ends here
