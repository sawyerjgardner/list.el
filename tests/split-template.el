;;; split-template.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: June 01, 2022
;; Modified: June 01, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/split-template
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require '+test)

;;tools

(defun +test-split--copy-dotted-list(list)
  ""
  (let* ((out  (cons nil nil))
         (spot out) )
    (while (consp list)
      (let ((next (cons (pop list) nil)))
        (setf (cdr spot) next
              spot       next )))
    (setf (cdr spot) list)
    (cdr out) ))

(defmacro +test-split--list-to-type(type arg)
  ""
  (if (eq type 'list)
      `(+test-split--copy-dotted-list ,arg)
    `(apply ,type ,arg) ))

;;tests

(defun +test-split-template--at-tests(_dotted)
  ""
  `((0 1 '(a b c) '(a)     '(b c))
    (1 2 '(a b c) '(a b)   '(c))
    (2 3 '(a b c) '(a b c) nil)
    (3 3 '(a b c) '(a b c) nil)
    (0 1 '(a)     '(a)     nil)
    (0 0 nil      nil      nil) ))

(defun +test-split-template--int-tests(_dotted)
  ""
  `((0   0  '(a b c) nil      '(a b c))
    (1   1  '(a b c) '(a)     '(b c))
    (2   2  '(a b c) '(a b)   '(c))
    (3   3  '(a b c) '(a b c) nil)
    (-1  0  '(a b c) nil      '(a b c))
    (4   3  '(a b c) '(a b c) nil)
    (0   0  nil      nil      nil)
    (-1  0  nil      nil      nil)
    (1   0  nil      nil      nil)
    (0   0  '(x)     nil      '(x))
    (-1  0  '(x)     nil      '(x))
    (1   1  '(x)     '(x)     nil)
    (nil 0  '(a b c) 'err     'err) ))

(defun +test-split-template--pred-tests(dotted)
  ""
  `((#'identity 0 '(a b c)   nil      '(a b c))
    (#'numberp  0 '(1 b 3)   nil      '(1 b 3))
    (#'numberp  1 '(a 2 c)   '(a)     '(2 c))
    (#'numberp  2 '(a b 3)   '(a b)   '(3))
    (#'ignore   3 '(a b c)   '(a b c) nil)
    (#'numberp  1 '(a 2)     '(a)     '(2))
    (#'identity 0 '(x)       nil      '(x))
    (#'ignore   1 '(x)       '(x)     nil)
    (#'error    0 nil        nil      nil)
    (#'error    0 '(x)       'err     'err)
    ,@(cond
       ((eq dotted 'error)
        `((#'identity 0 'x         'err     'err)
          (#'ignore   0 'x         'err     'err)
          (#'ignore   0 '(a b . c) 'err     'err)
          (#'numberp  1 '(a 2 . c) 'err     'err)
          (#'identity 0 '(a b . c) 'err     'err) ))
       (dotted
        `((#'identity 0 'x         'err     'err)
          (#'ignore   0 'x         'err     'err)
          (#'ignore   0 '(a b . c) 'err     'err)
          (#'numberp  1 '(a 2 . c) '(a)     '(2 . c))
          (#'identity 0 '(a b . c) nil      '(a b . c)) )))))

(defun +test-split-template--elt-tests(dotted)
  ""
  `(('(a)          0 '(a b c)   nil        '(a b c))
    ('(b)          1 '(a b c)   '(a)       '(b c))
    ('(c)          2 '(a b c)   '(a b)     '(c))
    ('(x)          3 '(a b c)   '(a b c)   nil)
    ('(x ,#'error) 0 nil        nil        nil)
    ('(x ,#'error) 0 '(x)       'err       'err)
    ('(x)          0 '(x)       nil        '(x))
    ('(a)          1 '(x)       '(x)       nil)
    ('((b))        1 '(a (b) c) '(a)       '((b) c))
    ('((b) ,#'eq)  3 '(a (b) c) '(a (b) c) nil)
    ('(x)          0 'x         'err       'err)
    ('(x)          0 '(a b . c) 'err       'err)
    ('(b)          0 '[a b c]   'err       'err)
    ,@(cond
       ((eq dotted 'error)
        `(('(b) 1 '(a b . c) 'err 'err)) )
       (dotted
        `(('(b) 1 '(a b . c) '(a) '(b . c))) ))))

(defun +test-split-template--cdr-tests(dotted)
  ""
  `((#'identity                   0 '(a b c)   nil      '(a b c))
    ((lambda(x) (= (length x) 3)) 0 '(a b c)   nil      '(a b c))
    ((lambda(x) (= (length x) 2)) 1 '(a b c)   '(a)     '(b c))
    ((lambda(x) (eq (car x) 'b))  1 '(a b c)   '(a)     '(b c))
    ((lambda(x) (= (length x) 1)) 2 '(a b c)   '(a b)   '(c))
    ((lambda(x) (= (length x) 0)) 3 '(a b c)   '(a b c) nil)
    (#'ignore                     3 '(a b c)   '(a b c) nil)
    (#'error                      0 nil        nil      nil)
    (#'error                      0 '(x)       'err     'err)
    (#'identity                   0 '(x)       nil      '(x))
    (#'ignore                     1 '(x)       '(x)     nil)
    (#'ignore                     0 'x         'err     'err)
    (#'ignore                     0 '(a . b)   'err     'err)
    (#'ignore                     0 '[a b c]   'err     'err)
    ,@(cond
       ((eq dotted 'error)
        `(((lambda(x) (eq (car x) 'b))  1 '(a b . c) 'err 'err)
          ((lambda(x) (eq x       'c))  2 '(a b . c) 'err 'err)
          (#'identity                   0 'x         'err 'err) ))
       (dotted
        `(((lambda(x) (eq (car x) 'b))  1 '(a b . c) '(a)   '(b . c))
          ((lambda(x) (eq x       'c))  2 '(a b . c) '(a b) 'c)
          (#'identity                   0 'x         nil    'x) )))))

(defun +test-split-template--tests(type dotted)
  ""
  (pcase-exhaustive type
    ('at   (+test-split-template--at-tests   dotted))
    ('int  (+test-split-template--int-tests  dotted))
    ('pred (+test-split-template--pred-tests dotted))
    ('elt  (+test-split-template--elt-tests  dotted))
    ('cdr  (+test-split-template--cdr-tests  dotted)) ))

;;main template

(defmacro +test-split-template(&rest body)
  "FLAGS.
:index-type [int,pred,cdr,elt]
:seq-type   [list,vector,...]
:const      [nil,t]
:dotted     [nil,t,error]

binds the varables (val index seq before after)

\(fn [FLAGS] &rest BODY)"
  (declare (indent defun))
  (let ((index-type 'int)
        (seq-type   'list)
        (const      nil)
        (dotted     nil)
        (base-seq    (make-symbol "base-seq"))
        (base-before (make-symbol "base-before"))
        (base-after  (make-symbol "base-after")) )
    (while (keywordp (car body))
      (let ((flag (pop body))
            (val  (pop body)) )
        (pcase flag
          (:index-type (setf index-type val))
          (:seq-type   (setf seq-type   val))
          (:const      (setf const      val))
          (:dotted     (setf dotted     val)) )))
    `(+test-test (val index ,base-seq ,base-before ,base-after)
       ,(+test-split-template--tests index-type dotted)
       (let ((seq    (+test-split--list-to-type ,seq-type ,base-seq))
             (before (+test-split--list-to-type ,seq-type ,base-before))
             (after  (+test-split--list-to-type ,seq-type ,base-after)) )
         (+test-assert-const ,(when const '(seq))
           ,@body )))))


(provide 'split-template)
;; (provide '+test-split)
;;; split-template.el ends here
