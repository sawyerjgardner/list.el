;;; +test.el --- Tools for runing tests -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: March 18, 2022
;; Modified: March 18, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/test-list
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Tools for runing tests
;;
;;; Code:

(eval-and-compile
  (require 'seq)
  (require 'cl-lib)
  (require 'ert) )

;; vars

(defvar +test-loops nil
  "Wrather to run tests that might never return." )

;; utilitys

(defun +test-clean-list(list)
  ""
  (let ((out nil))
    (while (consp list)
      (push (pop list) out) )
    (nreverse out) ))

(defmacro +test--maplist(varlist &rest body)
  "

\(fn (SYMBOL LIST) BODY...)"
  (declare (indent defun))
  (let ((return (make-symbol "return")))
    `(let (,return)
       (dolist (,(car varlist)
                ,(cadr varlist)
                (nreverse ,return)
                ,@(cddr varlist) )
         (push (progn ,@body) ,return) ))))

(defmacro +test--dolet(specs &rest body)
  "A mix of `let' and `dolist'.
binds VALS to ARGS for ech list of VALS in SPECS. the current list of VALS is
bound to VALS-ARGS.

\(fn (VALS-ARG (ARGS...) ((VALS...)...)) BODY...)"
  (declare (indent defun))
  (let ((vals-arg (nth 0 specs))
        (args     (nth 1 specs))
        (vals     (nth 2 specs)))
    `(dolist (,vals-arg (list ,@(+test--maplist (x vals)
                                  `(list ,@x) )))
       (apply (lambda(,@args) ,@body) ,vals-arg) )))

(defun +test--error(err &rest flags)
  "Signal +test--error with ERR and FLAGS."
  (declare (indent defun))
  (if (not (eq (car err) '+test--error))
      (signal '+test--error `(,@flags :error ,err))
    (signal '+test--error `(,@flags ,@(cdr err))) ))

(defmacro +test--addlist(place val)
  ""
  (gv-letplace (getter setter) place
    (macroexp-let2 macroexp-copyable-p v val
      (funcall setter `(append (if (listp ,v)
                                   ,v
                                 (list ,v) )
                               ,getter )))))

(defun +test--should-ignore-p(symbol)
  ""
  (eq (string-to-char (symbol-name symbol)) ?_) )

(defun +test--remove-ignore(seq)
  ""
  (seq-remove #'+test--should-ignore-p seq) )

;;tests

(defmacro +test-let-copy(vars &rest body)
  "Bind VARS to a deep copy of itself for BODY.

\(fn (VARS...) BODY...)"
  (declare (indent defun))
  `(let ,(+test--maplist(x vars)
           (list x `(copy-tree ,x)) )
     ,@body ))

(defmacro +test-assert-const(vars &rest body)
  "Assert that VARS are the same value before and after evaluating BODY.

\(fn (VARS...) BODY...)"
  (declare (indent defun))
  (let ((sets (+test--maplist(x vars)
                (list
                 (make-symbol (concat (symbol-name x) "-old"))
                 (make-symbol (concat (symbol-name x) "-alias"))
                 x ))))
    `(let (,@(mapcar #'cdr sets)
           ,@(+test--maplist(x sets)
               (list (car x) `(copy-tree ,(caddr x))) ))
       ,@body
       ,@(+test--maplist (x sets)
           `(unless (and (equal ,(caddr x) ,(car x))
                         (eq    ,(caddr x) ,(cadr x)) )
              (signal 'const-var-change
                      (list ',(caddr x)
                            ,(cadr x)
                            ,(caddr x) )))))))

(defmacro +test-error-state(track-args &rest body)
  "Track the values of TRACK-ARGS incase BODY throws an error."
  (declare (indent defun))
  `(condition-case err
       ,@body
     (t
      (+test--error err
        :args (,'\` ,(+test--maplist(arg track-args)
                       (if (memq arg '(&optional &rest))
                           arg
                         (list arg `(,'\, ,arg)) )))))))

(defmacro +test-test(args valss &rest body)
  "
VALS are evaluated before binding and before iterating of body.
VALS starting with an underscore (_) are ignored.

\(fn (ARGS...) ((VALS...)...) [FLAGS] &rest BODY)"
  (declare (indent defun))
  (let ((const-args nil)
        (copy-args  nil)
        (track-args args) )
    (while (keywordp (car body))
      (let ((key (pop body))
            (val (pop body)) )
        (pcase key
          (:copy-args  (+test--addlist copy-args  val))
          (:const-args (+test--addlist const-args val))
          (:track-args (+test--addlist track-args val))
          (_           (error "Unknown key %s" key)) )))
    `(+test--dolet (,(make-symbol "test") ,args ,valss)
       (+test-error-state ,(+test--remove-ignore track-args)
         (+test-let-copy ,(+test--remove-ignore copy-args)
           (+test-assert-const ,(+test--remove-ignore const-args)
             ,@body ))))))


(provide '+test)
;;; +test.el ends here
