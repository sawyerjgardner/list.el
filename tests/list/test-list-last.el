;;; test-list-last.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 14, 2022
;; Modified: April 14, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/test-list-last
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'ert)
(require '+test)
(require 'list/last)

;;last

(ert-deftest test-list-tail()
  (+test-test(t1 should-return)
    (('(1 2 3) nil)
     ('(1)     nil)
     (nil      nil)
     ('x       'err)
     ([a b]    'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (progn
          (should-error (list-tail t1))
          (should-error (list-last  t1 0))
          (let ((zero 0))
            (should-error (list-last t1 zero)) ))
      (should (equal (list-tail t1) should-return))
      (should (eq    (list-tail t1) (list-last t1 0)))
      (should (eq    (list-tail t1) (nthcdr (length t1) t1)))
      (let ((zero 0))
        ;;to avoid compiler-macro
        (should (eq  (list-tail t1) (list-last t1 zero))) ))))

(ert-deftest test-list-last1()
  (+test-test(t1 should-return)
    (('(1 2 3) '(3))
     ('(1)     '(1))
     (nil      nil)
     ('x       'err)
     ([a b]    'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (progn
          (should-error (list-last1 t1))
          (should-error (list-last  t1 1))
          (let ((one 1))
            (should-error (list-last t1 one)) ))
      (should (equal (list-last1 t1) should-return))
      (should (eq    (list-last1 t1) (list-last t1 1)))
      (should (eq    (list-last1 t1) (nthcdr (1- (length t1)) t1)))
      (let ((one 1))
        ;;to avoid compiler-macro
        (should (eq  (list-last1 t1) (list-last t1 one))) ))))

(ert-deftest test-list-last()
  (+test-test(n t1 should-return)
    ((nil '(a b)         '(b))
     (nil nil            nil)
     (nil '(a)           '(a))
     (nil '(a b c)       '(c))
     (nil '(a nil (c d)) '((c d)))
     (nil '(a nil)       '(nil))
     (-1  '(a b c)       nil)
     (0   '(a b c)       nil)
     (1   '(a b c)       '(c))
     (2   '(a b c)       '(b c))
     (3   '(a b c)       '(a b c))
     (4   '(a b c)       '(a b c))
     (0   'x             'err)
     (-1  'x             'err)
     (3   'x             'err)
     (nil (cons 'a 'b)   'err)
     (0   '(a b . c)     'err)
     (-1  '(a b . c)     'err)
     (2   '(a b . c)     'err)
     (nil '[a b]         'err)
     (nil '(a b . c)     'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last t1 n))
      (should (equal (list-last t1 n) should-return))
      (should (eq    (list-last t1 n) (nthcdr (- (length t1) (or n 1)) t1))) )))

(ert-deftest test-list-tail-setf()
  (+test-test(t1 val should-return)
    (('(1 2 3) nil    '(1 2 3))
     (nil      '(a b) '(a b))
     ('(1 2 3) '(a b) '(1 2 3 a b))
     ('(1 2 3) 'x     '(1 2 3 . x))
     ('(1 2)   'x     '(1 2 . x))
     ('(1)     'x     '(1 . x))
     (nil      'x     'x)
     ([1 2 3]  nil    'err)
     ([]       nil    'err)
     ('x       nil    'err) )
    :copy-args  (val t1)
    :const-args (val)
    (if (eq should-return 'err)
        (should-error (setf (list-tail t1)   val))
      (let ((len (length t1)))
        (should (eq (setf (list-tail t1) val) val))
        (should (eq (nthcdr len t1) val))
        (should (equal t1 should-return)) ))))

(ert-deftest test-list-last1-setf()
  (+test-test(t1 val should-return)
    (('(1 2 3) nil    '(1 2))
     (nil      '(a b) '(a b))
     ('(1 2 3) '(a b) '(1 2 a b))
     ('(1 2 3) 'x     '(1 2 . x))
     ('(1 2)   'x     '(1 . x))
     ('(1)     'x     'x)
     (nil      'x     'x)
     ([1 2 3]  nil    'err)
     ([]       nil    'err)
     ('x       nil    'err) )
    :copy-args  (val t1)
    :const-args (val)
    (if (eq should-return 'err)
        (progn
          (should-error (setf (list-last1 t1)   val))
          (should-error (setf (list-last  t1 1) val)) )
      (let ((len (length t1)))
        (should (eq (setf (list-last1 t1) val) val))
        (should (eq (nthcdr (1- len) t1) val))
        (should (equal t1 should-return)) ))))

(ert-deftest test-list-last-setf()
  (+test-test(n t1 val should-return)
    ((0   '(1 2 3)   '(a b) '(1 2 3 a b))
     (1   '(1 2 3)   '(a b) '(1 2 a b))
     (nil '(1 2 3)   '(a b) '(1 2 a b))
     (2   '(1 2 3)   '(a b) '(1 a b))
     (3   '(1 2 3)   '(a b) '(a b))
     (4   '(1 2 3)   '(a b) '(a b))
     (0   nil        '(a b) '(a b))
     (1   nil        '(a b) '(a b))
     (1   nil        'x     'x)
     (1   '(1 2 3)   'x     '(1 2 . x))
     (4   '(1 2 3)   'x     'x)
     (0   '(1 2 3)   nil    '(1 2 3))
     (nil '(1 2 3)   nil    '(1 2))
     (2   '(1 2 3)   nil    '(1))
     (3   '(1 2 3)   nil    nil)
     (0   '(1 2 . 3) '(a b) 'err)
     (0   'x         '(a b) 'err)
     (0   [1 2 3]    '(a b) 'err) )
    :copy-args  (n val t1)
    :const-args (n val)
    (if (eq should-return 'err)
        (should-error (setf (list-last t1 n) val))
      (let ((len (length t1))
            (als t1))
        (should (eq    (setf (list-last t1 n) val) val))
        (should (equal t1 should-return))
        (should (eq    (nthcdr (- len (or n 1)) t1) val))
        (should (or (eq t1 als) (eq t1 val))) ))))

;;last before cdr

(ert-deftest test-list-last-before-cdr1()
  (+test-test(pred t1 should-return)
    ((#'ignore                     '(1 2 3)     '(3))
     (#'identity                   '(1 2 3)     '(1 2 3))
     (#'identity                   '(1)         '(1))
     ((lambda(x) (= (length x) 1)) '(1 2 3)     '(2 3))
     ((lambda(x) (= (length x) 2)) '(1 2 3)     '(1 2 3))
     ((lambda(x) (= (length x) 3)) '(1 2 3)     '(1 2 3))
     ((lambda(x) (= (length x) 4)) '(1 2 3)     '(3))
     (#'error                      nil          nil)
     (#'error                      '(1)         'err)
     ((lambda(x) (car x))          '(nil nil 2) '(nil 2))
     ((lambda(x) (not (consp x)))  '(a b . c)   '(b . c))
     ((lambda(x) (not (consp x)))  'c           'c)
     (#'ignore                     '(a b . c)   'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last-before-cdr1 t1 pred))
      (let ((return (list-last-before-cdr1 t1 pred))
            (idx (let ((idx  0)
                       (spot t1) )
                   (while (and (consp spot)
                               (not (funcall pred spot)) )
                     (list-pop spot)
                     (setq idx (1+ idx)) )
                   (1- idx) )))
        (should (equal return should-return))
        (should (eq    return (nthcdr idx t1))) ))))

(ert-deftest test-list-last-before-cdr()
  (+test-test(pred n t1 should-return)
    ((#'ignore                  0   '(1 2 3)     nil)
     (#'ignore                  -1  '(1 2 3)     nil)
     (#'ignore                  1   '(1 2 3)     '(3))
     (#'ignore                  nil '(1 2 3)     '(3))
     (#'ignore                  2   '(1 2 3)     '(2 3))
     (#'ignore                  3   '(1 2 3)     '(1 2 3))
     (#'ignore                  4   '(1 2 3)     '(1 2 3))
     (#'ignore                  0   '(1)         nil)
     (#'ignore                  1   '(1)         '(1))
     (#'ignore                  nil nil          nil)
     (#'identity                nil '(a . b)     '(a . b))
     (#'identity                nil 'x           'x)
     ((lambda(x) (not (car x))) nil '(1 2 nil)   '(2 nil))
     ((lambda(x) (not (car x))) nil '(nil 2 nil) '(nil 2 nil))
     ((lambda(x) (not (car x))) 0   '(1 2 nil)   '(nil))
     ((lambda(x) (not (car x))) 2   '(1 2 nil)   '(1 2 nil))
     ((lambda(x) (not (car x))) 2   '(1 2 3 nil) '(2 3 nil))
     ((lambda(x) (not (car x))) 1   '(1 2 nil 3) '(2 nil 3)) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last-before-cdr t1 pred n))
      (let ((return (list-last-before-cdr t1 pred n))
            (idx (let ((idx  0)
                       (spot t1) )
                   (while (and (consp spot)
                               (not (funcall pred spot)) )
                     (list-pop spot)
                     (setq idx (1+ idx)) )
                   (- idx (or n 1)) )))
        (should (equal return should-return))
        (should (eq    return (nthcdr idx t1))) ))))

(ert-deftest test-list-last-before-cdr1-setf()
  (+test-test(pred t1 val should-return)
    ((#'ignore                    '(a b c)       '(1 2) '(a b 1 2))
     (#'ignore                    '(a b c)       nil    '(a b))
     (#'ignore                    nil            '(1 2) '(1 2))
     (#'ignore                    '(a)           'x     'x)
     ((lambda(x) (not (car x)))   '(a b nil)     '(x y) '(a x y))
     ((lambda(x) (not (car x)))   '(nil b c)     '(x y) '(x y))
     (#'error                     nil            '(x y) '(x y))
     ((lambda(x) (not (car x)))   '(nil . b)     '(x y) '(x y))
     (#'ignore                    'x             '(x y) 'err)
     ((lambda(x) (not (car x)))   '(a . b)       '(x y) 'err)
     ((lambda(x) (not (car x)))   '(a nil . c)   '(x y) '(x y))
     ((lambda(x) (not (car x)))   '(a b nil . d) '(x y) '(a x y))
     ((lambda(x) (not (consp x))) '(a b . c)     '(x y) '(a x y)) )
    :copy-args  (pred t1 val)
    :const-args (pred    val)
    (if (eq should-return 'err)
        (should-error (setf (list-last-before-cdr1 t1 pred) val))
      (let ((old t1)
            (idx (let ((idx  0)
                       (spot t1) )
                   (while (and (consp spot)
                               (not (funcall pred spot)) )
                     (list-pop spot)
                     (setq idx (1+ idx)) )
                   (1- idx) )))
        (should (eq    (setf (list-last-before-cdr1 t1 pred) val) val))
        (should (equal t1 should-return))
        (when (and old (> idx 0))
          (should (eq old t1)) )
        (should (eq (nthcdr idx t1) val)) ))))

(ert-deftest test-list-last-before-cdr-setf()
  (+test-test(pred n t1 val should-return)
    ((#'ignore                  0   '(a b c)       '(1 2) '(a b c 1 2))
     (#'ignore                  nil '(a b c)       '(1 2) '(a b 1 2))
     (#'ignore                  1   '(a b c)       '(1 2) '(a b 1 2))
     (#'ignore                  2   '(a b c)       '(1 2) '(a 1 2))
     (#'ignore                  3   '(a b c)       '(1 2) '(1 2))
     (#'ignore                  4   '(a b c)       '(1 2) '(1 2))
     (#'ignore                  0   '(a b c)       nil    '(a b c))
     (#'ignore                  nil '(a b c)       nil    '(a b))
     (#'ignore                  5   '(a b c)       nil    nil)
     (#'error                   0   nil            '(1 2) '(1 2))
     (#'ignore                  nil nil            '(1 2) '(1 2))
     (#'ignore                  1   '(a b c)       'x     '(a b . x))
     (#'ignore                  nil '(a)           'x     'x)
     ((lambda(x) (not (car x))) -3  '(a nil c)     '(x y) '(a x y))
     ((lambda(x) (not (car x))) nil '(a b nil)     nil    '(a))
     ((lambda(x) (not (car x))) 5   '(a nil c)     nil    nil)
     ((lambda(x) (not (car x))) 1   '(a b nil)     'x     '(a . x))
     ((lambda(x) (not (car x))) nil '(a nil)       'x     'x)
     ((lambda(x) (not (car x))) 0   '(nil b c)     nil    nil)
     ((lambda(x) (not (car x))) 0   '(nil . b)     '(x y) '(x y))
     ((lambda(x) (not (car x))) nil 'x             '(x y) 'err)
     ((lambda(x) (not (car x))) nil '(a . b)       '(x y) 'err)
     ((lambda(x) (not (car x))) -1  '(a nil . c)   '(x y) '(a x y))
     ((lambda(x) (not (car x))) 2   '(a b nil . d) '(x y) '(x y))
     )
    :copy-args  (pred n t1 val)
    :const-args (pred n    val)
    (if (eq should-return 'err)
        (should-error (setf (list-last-before-cdr t1 pred n) val))
      (let ((old t1)
            (idx (let ((idx  0)
                       (spot t1) )
                   (while (and (consp spot)
                               (not (funcall pred spot)) )
                     (list-pop spot)
                     (setq idx (1+ idx)) )
                   (- idx (max (or n 1) 0)) )))
        (should (eq    (setf (list-last-before-cdr t1 pred n) val) val))
        (should (equal t1 should-return))
        (when (and old (> idx 0))
          (should (eq old t1)) )
        (should (eq (nthcdr idx t1) val)) ))))

;;last before

(ert-deftest test-list-last-before1()
  (+test-test(pred t1 should-return)
    ((#'ignore  '(1 2 3)   '(3))
     (#'ignore  nil        nil)
     (#'numberp '(a b 1)   '(b 1))
     (#'numberp '(a 1 c)   '(a 1 c))
     (#'numberp '(1 b c)   '(1 b c))
     (#'numberp '(1)       '(1))
     (#'numberp '(a 1)     '(a 1))
     (#'numberp '(a b)     '(b))
     (#'error   '(a b)     'err)
     (#'error   nil        nil)
     (#'not     '(a b c)   '(c))
     (#'not     '(a nil)   '(a nil))
     (#'not     '(nil a)   '(nil a))
     (#'not     nil        nil)
     (#'ignore  '(nil a)   '(a))
     (#'ignore  '(a nil)   '(nil))
     (#'numberp '(nil b 1) '(b 1)) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last-before1 t1 pred))
      (let ((return (list-last-before1 t1 pred)))
        (should (equal return should-return))
        (should (eq    return (nthcdr (1- (or (seq-position t1 nil (lambda(x _)
                                                                     (funcall pred x) ))
                                              (length t1) ))
                                      t1 )))))))

(ert-deftest test-list-last-before()
  (+test-test(pred n t1 should-return)
    ((#'ignore  0   '(1 2 3)   nil)
     (#'ignore  nil '(1 2 3)   '(3))
     (#'ignore  1   '(1 2 3)   '(3))
     (#'ignore  2   '(1 2 3)   '(2 3))
     (#'ignore  3   '(1 2 3)   '(1 2 3))
     (#'ignore  4   '(1 2 3)   '(1 2 3))
     (#'ignore  0   nil        nil)
     (#'ignore  nil nil        nil)
     (#'numberp 0   '(a b 1)   '(1))
     (#'numberp nil '(a b 1)   '(b 1))
     (#'numberp nil '(a 1 c)   '(a 1 c))
     (#'numberp 0   '(a 1 c)   '(1 c))
     (#'numberp nil '(1 b c)   '(1 b c))
     (#'numberp 0   '(1 b 1)   '(1 b 1))
     (#'numberp 2   '(a b 1 d) '(a b 1 d))
     (#'numberp 1   '(a b 1 d) '(b 1 d))
     (#'numberp 2   '(a b c 1) '(b c 1))
     (#'numberp -1  '(a 1 c d) '(1 c d))
     (#'numberp -1  '(a b c 1) '(1))
     (#'not     nil '(nil 1 c) '(nil 1 c))
     (#'not     nil '(a nil c) '(a nil c))
     (#'numberp 0   '(1 a . b) '(1 a . b))
     (#'numberp 0   '(a 1 . b) '(1 . b))
     (#'numberp 0   '(a b . 1) 'err)
     (#'numberp 0   '1         'err)
     (#'numberp 0   [1 2 3]    'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last-before t1 pred n))
      (let ((return (list-last-before t1 pred n)))
        (should (equal return should-return))
        (should (eq    return
                       (nthcdr (- (or (seq-position (+test-clean-list t1)
                                                    nil
                                                    (lambda(x _)
                                                      (funcall pred x) ))
                                      (safe-length t1) )
                                  (max (or n 1) 0) )
                               t1 )))))))

(ert-deftest test-list-last-before1-setf()
  (+test-test(pred t1 val should-return)
    ((#'ignore  '(a b c)     '(1 2) '(a b 1 2))
     (#'ignore  '(a b c)     nil    '(a b))
     (#'ignore  nil          '(1 2) '(1 2))
     (#'ignore  '(a)         'x     'x)
     (#'numberp '(a b 1)     '(x y) '(a x y))
     (#'numberp '(1 b c)     '(x y) '(x y))
     (#'error   nil          '(x y) '(x y))
     (#'numberp '(1 . b)     '(x y) '(x y))
     (#'ignore  'x           '(x y) 'err)
     (#'numberp '(a . b)     '(x y) 'err)
     (#'numberp '(a 1 . c)   '(x y) '(x y))
     (#'numberp '(a b 1 . d) '(x y) '(a x y)) )
    :copy-args  (pred t1 val)
    :const-args (pred    val)
    (if (eq should-return 'err)
        (should-error (setf (list-last-before1 t1 pred) val))
      (let ((old t1)
            (len (1- (or (seq-position (+test-clean-list t1)
                                       nil
                                       (lambda(x _)
                                         (funcall pred x) ))
                         (safe-length t1) ))))
        (should (eq    (setf (list-last-before1 t1 pred) val) val))
        (should (equal t1 should-return))
        (when (and old (> len 0))
          (should (eq old t1)) )
        (should (eq (nthcdr len t1) val)) ))))

(ert-deftest test-list-last-before-setf()
  (+test-test(pred n t1 val should-return)
    ((#'ignore  0   '(a b c)     '(1 2) '(a b c 1 2))
     (#'ignore  nil '(a b c)     '(1 2) '(a b 1 2))
     (#'ignore  1   '(a b c)     '(1 2) '(a b 1 2))
     (#'ignore  2   '(a b c)     '(1 2) '(a 1 2))
     (#'ignore  3   '(a b c)     '(1 2) '(1 2))
     (#'ignore  4   '(a b c)     '(1 2) '(1 2))
     (#'ignore  0   '(a b c)     nil    '(a b c))
     (#'ignore  nil '(a b c)     nil    '(a b))
     (#'ignore  5   '(a b c)     nil    nil)
     (#'ignore  0   nil          '(1 2) '(1 2))
     (#'ignore  nil nil          '(1 2) '(1 2))
     (#'ignore  1   '(a b c)     'x     '(a b . x))
     (#'ignore  nil '(a)         'x     'x)
     (#'numberp -3  '(a 1 c)     '(x y) '(a x y))
     (#'numberp nil '(a b 1)     nil    '(a))
     (#'numberp 5   '(a 1 c)     nil    nil)
     (#'numberp 1   '(a b 1)     'x     '(a . x))
     (#'numberp nil '(a 1)       'x     'x)
     (#'numberp 0   '(1 b c)     nil    nil)
     (#'numberp 0   '(1 . b)     '(x y) '(x y))
     (#'ignore  nil 'x           '(x y) 'err)
     (#'numberp nil '(a . b)     '(x y) 'err)
     (#'numberp -1  '(a 1 . c)   '(x y) '(a x y))
     (#'numberp 2   '(a b 1 . d) '(x y) '(x y)) )
    :copy-args  (pred n t1 val)
    :const-args (pred n    val)
    (if (eq should-return 'err)
        (should-error (setf (list-last-before t1 pred n) val))
      (let ((old t1)
            (len (- (or (seq-position (+test-clean-list t1)
                                      nil
                                      (lambda(x _)
                                        (funcall pred x) ))
                        (safe-length t1) )
                    (max (or n 1) 0) )))
        (should (eq    (setf (list-last-before t1 pred n) val) val))
        (should (equal t1 should-return))
        (when (and old (> len 0))
          (should (eq old t1)) )
        (should (eq (nthcdr len t1) val)) ))))

;;last before elt

(ert-deftest test-list-last-before-elt1()
  (+test-test(testfn elt t1 should-return)
    ((nil     'x     '(a b c)       '(c))
     (nil     1      '(a b 1)       '(b 1))
     (nil     1      '(a 1 c)       '(a 1 c))
     (nil     1      '(1 b c)       '(1 b c))
     (nil     'x     '(x)           '(x))
     (nil     'x     '(a)           '(a))
     (nil     'x     '(a x)         '(a x))
     (#'eq    '(x y) '(x y (x y) z) '(z))
     (nil     '(x y) '(x y (x y) z) '(y (x y) z))
     (#'error 'x     nil            nil)
     (#'error 'x     '(a b)         'err)
     (nil     'x     '(a . b)       'err)
     (nil     'x     '(a x . b)     '(a x . b))
     (nil     'x     '[a b c]       'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last-before-elt1 t1 elt testfn))
      (let ((return (list-last-before-elt1 t1 elt testfn)))
        (should (equal return should-return))
        (should (eq    return (nthcdr (1- (or (seq-position (+test-clean-list t1)
                                                            elt testfn )
                                              (length t1) ))
                                      t1 )))))))

(ert-deftest test-list-last-before-elt()
  (+test-test(testfn elt n t1 should-return)
    ((nil     'x     0   '(1 2 3)   nil)
     (nil     'x     nil '(1 2 3)   '(3))
     (nil     'x     1   '(1 2 3)   '(3))
     (nil     'x     2   '(1 2 3)   '(2 3))
     (nil     'x     3   '(1 2 3)   '(1 2 3))
     (nil     'x     4   '(1 2 3)   '(1 2 3))
     (#'error 'x     0   nil        nil)
     (#'error 'x     nil nil        nil)
     (nil     1      0   '(a b 1)   '(1))
     (nil     1      nil '(a b 1)   '(b 1))
     (nil     1      nil '(a 1 c)   '(a 1 c))
     (nil     1      0   '(a 1 c)   '(1 c))
     (nil     1      nil '(1 b c)   '(1 b c))
     (nil     1      0   '(1 b 1)   '(1 b 1))
     (nil     1      2   '(a b 1 d) '(a b 1 d))
     (nil     1      1   '(a b 1 d) '(b 1 d))
     (nil     1      2   '(a b c 1) '(b c 1))
     (nil     1      -1  '(a 1 c d) '(1 c d))
     (nil     1      -1  '(a b c 1) '(1))
     (nil     nil    nil '(nil 1 c) '(nil 1 c))
     (nil     nil    nil '(a nil c) '(a nil c))
     (nil     nil    nil '(a b nil) '(b nil))
     (#'eq    '(x y) 0   '(x (x y) y) nil)
     (nil     '(x y) 0   '(x (x y) y) '((x y) y))
     (nil     1      0   '(1 a . b) '(1 a . b))
     (nil     1      0   '(a 1 . b) '(1 . b))
     (nil     1      0   '(a b . 1) 'err)
     (nil     1      0   '1         'err)
     (nil     1      0   [1 2 3]    'err) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-last-before-elt t1 elt n testfn))
      (let ((return (list-last-before-elt t1 elt n testfn)))
        (should (equal return should-return))
        (should (eq    return
                       (nthcdr (- (or (seq-position (+test-clean-list t1)
                                                    elt testfn )
                                      (safe-length t1) )
                                  (max (or n 1) 0) )
                               t1 )))))))

(ert-deftest test-list-last-before-elt1-setf()
  (+test-test(testfn elt t1 val should-return)
    ((nil     'x '(a b c)     '(1 2) '(a b 1 2))
     (nil     'x '(a b c)     nil    '(a b))
     (#'error 'x nil          '(1 2) '(1 2))
     (nil     'x '(a)         'x     'x)
     (nil     1  '(a b 1)     '(x y) '(a x y))
     (nil     1  '(1 b c)     '(x y) '(x y))
     (nil     1  '(1 . b)     '(x y) '(x y))
     (nil     'x 'x           '(x y) 'err)
     (nil     'x '(a . b)     '(x y) 'err)
     (nil     1  '(a 1 . c)   '(x y) '(x y))
     (nil     1  '(a b 1 . d) '(x y) '(a x y)) )
    :copy-args  (testfn elt t1 val)
    :const-args (testfn elt    val)
    (if (eq should-return 'err)
        (should-error (setf (list-last-before-elt1 t1 elt testfn) val))
      (let ((old t1)
            (len (1- (or (seq-position (+test-clean-list t1)
                                       elt testfn )
                         (safe-length t1) ))))
        (should (eq    (setf (list-last-before-elt1 t1 elt testfn) val) val))
        (should (equal t1 should-return))
        (when (and old (> len 0))
          (should (eq old t1)) )
        (should (eq (nthcdr len t1) val)) ))))

(ert-deftest test-list-last-before-elt-setf()
  (+test-test(testfn elt n t1 val should-return)
    ((nil     'x     0   '(a b c)     '(1 2) '(a b c 1 2))
     (nil     'x     nil '(a b c)     '(1 2) '(a b 1 2))
     (nil     'x     1   '(a b c)     '(1 2) '(a b 1 2))
     (nil     'x     2   '(a b c)     '(1 2) '(a 1 2))
     (nil     'x     3   '(a b c)     '(1 2) '(1 2))
     (nil     'x     4   '(a b c)     '(1 2) '(1 2))
     (nil     'x     0   '(a b c)     nil    '(a b c))
     (nil     'x     nil '(a b c)     nil    '(a b))
     (nil     'x     5   '(a b c)     nil    nil)
     (nil     'x     0   nil          '(1 2) '(1 2))
     (#'error 'x     nil nil          '(1 2) '(1 2))
     (nil     'x     1   '(a b c)     'x     '(a b . x))
     (nil     'x     nil '(a)         'x     'x)
     (nil     1      -3  '(a 1 c)     '(x y) '(a x y))
     (nil     1      nil '(a b 1)     nil    '(a))
     (nil     1      5   '(a 1 c)     nil    nil)
     (nil     1      1   '(a b 1)     'x     '(a . x))
     (nil     1      nil '(a 1)       'x     'x)
     (nil     1      0   '(1 b c)     nil    nil)
     (nil     1      0   '(1 . b)     '(x y) '(x y))
     (nil     1      nil 'x           '(x y) 'err)
     (nil     1      nil '(a . b)     '(x y) 'err)
     (nil     1      -1  '(a 1 . c)   '(x y) '(a x y))
     (nil     1      2   '(a b 1 . d) '(x y) '(x y))
     (#'eq    '(x y) 0   '(x (x y) y) nil    '(x (x y) y))
     (nil     '(x y) 0   '(x (x y) y) nil    '(x)) )
    :copy-args  (testfn elt n t1 val)
    :const-args (testfn elt n    val)
    (if (eq should-return 'err)
        (should-error (setf (list-last-before-elt t1 elt n testfn) val))
      (let ((old t1)
            (len (- (or (seq-position (+test-clean-list t1)
                                      elt testfn )
                        (safe-length t1) )
                    (max (or n 1) 0) )))
        (should (eq    (setf (list-last-before-elt t1 elt n testfn) val) val))
        (should (equal t1 should-return))
        (when (and old (> len 0))
          (should (eq old t1)) )
        (should (eq (nthcdr len t1) val)) ))))

(provide 'test-list-last)
;; (provide 'test-list)
;;; test-list-last.el ends here
