;;; test-list-take.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 23, 2022
;; Modified: April 23, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/test-list-drop
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


(require 'ert)
(require '+test)
(require 'split-template)
(require 'list/take)

(defmacro test-list-take-template(type destructive command)
  ""
  (declare (indent defun))
  `(ert-deftest ,(intern (concat "test-" (symbol-name (car command))))()
     (+test-split-template
       :index-type ,type
       :seq-type   list
       :const      ,(not destructive)
       :dotted     t
       (ignore after)
       (message "hoof: %s %s" before after)
       (if (eq before 'err)
           (should-error ,command)
         (let ((return ,command))
           (should (equal return before))
           ,(if destructive
                `(when return
                   (should (eq return seq)) )
              `(when seq
                 (should-not (eq return seq)) )))))))

(test-list-take-template at   t   (list-ntake-region seq (nthcdr val seq)))
(test-list-take-template at   nil (list-take-region  seq (nthcdr val seq)))

(test-list-take-template int  t   (list-ntake           seq val))
(test-list-take-template pred t   (list-ntake-until     seq val))
(test-list-take-template elt  t   (list-ntake-until-elt seq (car val) (cadr val)))
(test-list-take-template cdr  t   (list-ntake-until-cdr seq val))

(test-list-take-template int  nil (list-take           seq val))
(test-list-take-template pred nil (list-take-until     seq val))
(test-list-take-template elt  nil (list-take-until-elt seq (car val) (cadr val)))
(test-list-take-template cdr  nil (list-take-until-cdr seq val))

;;take setf

(defmacro test-list--take-setf-test(command type index-command)
  "
uses should-return, val, and t1"
  (declare (indent defun))
  `(if (eq should-return 'err)
       (should-error (setf ,command val))
     (let ((rest (nthcdr ,index-command t1))
           (als   t1)
           (len   (safe-length t1)) )
       (should (eq    (setf ,command val) val))
       (should (equal t1 should-return))
       ,@(pcase-exhaustive type
           ('n   `((ignore als len)
                   (if (equal t1 val)
                       (should (eq t1 val))
                     (should (eq (nthcdr (safe-length val) t1) rest)) )))
           ('nil `((ignore len rest)
                   (when t1
                     ;;TODO: maybe dont copy val?
                     (should-not (eq t1 val))
                     (should-not (eq t1 als))
                     (when rest
                       (should-not (eq t1 rest))
                       (should-not (eq (nthcdr (safe-length val) t1) rest)) ))))))))

(defmacro test-list-take-region-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(n t1 val should-return)
       ((0 '(a b c) '(8 9) '(8 9 b c))
        (1 '(a b c) '(8 9) '(8 9 c))
        (2 '(a b c) '(8 9) '(8 9))
        (3 '(a b c) '(8 9) '(8 9))
        (0 '(a b c) nil    '(b c))
        (1 '(a b c) nil    '(c))
        (2 '(a b c) nil    nil)
        (3 '(a b c) nil    nil)
        (0 nil      nil    nil)
        (0 nil      '(8 9) '(8 9)) )
       :copy-args  (n t1 val)
       :const-args (n    val)
       (test-list--take-setf-test (,fn t1 (nthcdr n t1)) ,type
         (1+ n) ))))


(defmacro test-list-take-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(n t1 val should-return)
       (
        (0  '(1 2 3) '(a b) '(a b 1 2 3))
        (1  '(1 2 3) '(a b) '(a b 2 3))
        (2  '(1 2 3) '(a b) '(a b 3))
        (3  '(1 2 3) '(a b) '(a b))
        (4  '(1 2 3) '(a b) 'err) ;;TODO: maybe remove error?
        (-1 '(1 2 3) '(a b) 'err)
        (0  '(1 2 3) nil    '(1 2 3))
        (1  '(1 2 3) nil    '(2 3))
        (3  '(1 2 3) nil    nil)
        (0  nil      nil    nil)
        (0  nil      '(a b) '(a b))
        (1 'x        '(1 2) 'err)
        (2 '(a . b)  '(1 2) 'err)
        (1 '(a b)    'x     'err)
        ,@(pcase-exhaustive type
            ('n
             `((0 'x       '(1 2) '(1 2 . x))
               (0 '(a . b) '(1 2) '(1 2 a . b))
               (1 '(a . b) '(1 2) '(1 2 . b))
               (1 '(a . b) nil    'b)
               (0 nil      'a     'a)
               (2 '(a b)   'x     'x) ))
            ('nil
             `((0 'x       '(1 2) 'err)
               (0 '(a . b) '(1 2) 'err)
               (1 '(a . b) '(1 2) 'err)
               (1 '(a . b) nil    'err)
               (0 nil      'a     'err)
               (2 '(a b)   'x     'err) ))))
       :copy-args  (n t1 val)
       :const-args (n    val)
       (test-list--take-setf-test (,fn t1 n) ,type
         n ))))

(defmacro test-list-take-until-cdr-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(pred t1 val should-return)
       ((#'identity                  '(a b c) '(8 9) '(8 9 a b c))
        ((lambda(x) (eq (car x) 'a)) '(a b c) '(8 9) '(8 9 a b c))
        ((lambda(x) (eq (car x) 'b)) '(a b c) '(8 9) '(8 9 b c))
        ((lambda(x) (eq (car x) 'c)) '(a b c) '(8 9) '(8 9 c))
        (#'ignore                    '(a b c) '(8 9) '(8 9))
        (#'identity                  '(a b c) nil    '(a b c))
        ((lambda(x) (eq (car x) 'b)) '(a b c) nil    '(b c))
        (#'ignore                    '(a b c) nil    nil)
        (#'error                     nil      nil    nil)
        (#'error                     nil      '(8 9) '(8 9))
        (#'ignore                    'x       '(8 9) 'err)
        (#'ignore                    '(a . b) '(8 9) 'err)
        (#'identity                  '(a b)   'x     'err)
        ,@(pcase-exhaustive type
            ('n
             `((#'ignore   '(a b) 'x   'x)
               (#'identity 'y     '(x) '(x . y))
               (#'identity 'y     nil  'y) ))
            ('nil
             `((#'ignore   '(a b) 'x   'err)
               (#'identity 'y     '(x) 'err)
               (#'identity 'y     nil  'err) ))))
       :copy-args  (pred t1 val)
       :const-args (pred    val)
       (test-list--take-setf-test (,fn t1 pred) ,type
         (let ((idx  0)
               (spot t1) )
           (while (and (consp spot)
                       (not (funcall pred spot)) )
             (list-pop spot)
             (setq idx (1+ idx)) )
           idx )))))

(defmacro test-list-take-until-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(pred t1 val should-return)
       ((#'identity            '(a b c) '(8 9) '(8 9 a b c))
        ((lambda(x) (eq x 'a)) '(a b c) '(8 9) '(8 9 a b c))
        ((lambda(x) (eq x 'b)) '(a b c) '(8 9) '(8 9 b c))
        ((lambda(x) (eq x 'c)) '(a b c) '(8 9) '(8 9 c))
        (#'ignore              '(a b c) '(8 9) '(8 9))
        (#'identity            '(a b c) nil    '(a b c))
        ((lambda(x) (eq x 'b)) '(a b c) nil    '(b c))
        (#'ignore              '(a b c) nil    nil)
        (#'error               nil      nil    nil)
        (#'error               nil      '(8 9) '(8 9))
        (#'ignore              'x       '(8 9) 'err)
        (#'ignore              '(a . b) '(8 9) 'err)
        (#'identity            '(a b)   'x     'err)
        ,@(pcase-exhaustive type
            ('n
             `((#'ignore   '(a b) 'x   'x)
               (#'identity 'y     '(x) 'err)
               (#'identity 'y     nil  'err) ))
            ('nil
             `((#'ignore   '(a b) 'x   'err)
               (#'identity 'y     '(x) 'err)
               (#'identity 'y     nil  'err) ))))
       :copy-args  (pred t1 val)
       :const-args (pred    val)
       (test-list--take-setf-test (,fn t1 pred) ,type
         (or (seq-position (+test-clean-list t1)
                           nil
                           (lambda(x _)
                             (funcall pred x) ))
             (safe-length t1) )))))

;;TODO: more tests
(defmacro test-list-take-until-elt-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(elt t1 val should-return &optional testfn)
       (('a '(a b c) '(8 9) '(8 9 a b c))
        ('b '(a b c) '(8 9) '(8 9 b c))
        ('c '(a b c) '(8 9) '(8 9 c))
        ('x '(a b c) '(8 9) '(8 9))
        ('a '(a b c) nil    '(a b c))
        ('b '(a b c) nil    '(b c))
        ('x '(a b c) nil    nil)
        ('x nil      nil    nil    #'error)
        ('x nil      '(8 9) '(8 9) #'error)
        ('x 'x       '(8 9) 'err)
        ('x '(a . b) '(8 9) 'err)
        ,@(pcase-exhaustive type
            ('n
             `(('x '(a b) 'x   'x)
               ('x 'y     '(x) 'err)
               ('x 'y     nil  'err) ))
            ('nil
             `(('x '(a b) 'x   'err)
               ('x 'y     '(x) 'err)
               ('x 'y     nil  'err) ))))
       :copy-args  (elt t1 val testfn)
       :const-args (elt    val testfn)
       (test-list--take-setf-test (,fn t1 elt testfn) ,type
         (or (seq-position (+test-clean-list t1)
                           elt testfn )
             (safe-length t1) )))))


(test-list-take-region-setf-template list-ntake-region n)
(test-list-take-region-setf-template list-take-region)

(test-list-take-setf-template list-ntake n)
(test-list-take-setf-template list-take)

(test-list-take-until-cdr-setf-template list-ntake-until-cdr n)
(test-list-take-until-cdr-setf-template list-take-until-cdr)

(test-list-take-until-setf-template list-ntake-until n)
(test-list-take-until-setf-template list-take-until)

(test-list-take-until-elt-setf-template list-ntake-until-elt n)
(test-list-take-until-elt-setf-template list-take-until-elt)


(provide 'test-list-take)
;; (provide 'test-list)
;;; test-list-take.el ends here
