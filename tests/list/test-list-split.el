;;; test-list-split.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: May 14, 2022
;; Modified: May 14, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/test-list-split
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'ert)
(require '+test)
(require 'split-template)
(require 'list/split)

(defmacro test-list-split-template(type destruct-type command)
  ""
  (declare (indent defun))
  `(ert-deftest ,(intern (concat "test-" (symbol-name (car command))))()
     (+test-split-template
       :index-type ,type
       :seq-type   list
       :const      ,(not (eq destruct-type 'n))
       :dotted     ,(if (member destruct-type '(n *)) t 'error)
       (if (member 'err (list before after))
           (should-error ,command)
         (let ((old-after   (nthcdr index seq))
               (return ,command))
           (should (equal return (list before after)))
           ,(pcase-exhaustive destruct-type
              ;;TODO: chamge tests
              ('n   `(progn
                       (when (nth 0 return)
                         (should (eq (nth 0 return) seq)) )
                       (should (eq (nth 1 return) old-after)) ))
              ('*   `(progn
                       (when (nth 0 return)
                         (if (nth 1 return)
                             (should-not (eq (nth 0 return) seq))
                           (should (eq (nth 0 return) seq)) ))
                       (should (eq (nth 1 return) old-after)) ))
              ('**  `(progn
                       (when (nth 0 return)
                         (should-not (eq (nth 0 return) seq)) )
                       (when (nth 1 return)
                         (should-not (eq (nth 1 return) old-after)) )))))))))

(test-list-split-template at   n  (list-nsplit-when-after seq (nthcdr val seq)))
(test-list-split-template int  n  (list-nsplit            seq val))
(test-list-split-template pred n  (list-nsplit-when       seq val))
(test-list-split-template cdr  n  (list-nsplit-when-cdr   seq val))
(test-list-split-template elt  n  (list-nsplit-when-elt   seq (car val) (cadr val)))

(test-list-split-template at   *  (list-split-when-after* seq (nthcdr val seq)))
(test-list-split-template int  *  (list-split*            seq val))
(test-list-split-template pred *  (list-split-when*       seq val))
(test-list-split-template cdr  *  (list-split-when-cdr*   seq val))
(test-list-split-template elt  *  (list-split-when-elt*   seq (car val) (cadr val)))

(test-list-split-template at   ** (list-split-when-after** seq (nthcdr val seq)))
(test-list-split-template int  ** (list-split**            seq val))
(test-list-split-template pred ** (list-split-when**       seq val))
(test-list-split-template cdr  ** (list-split-when-cdr**   seq val))
(test-list-split-template elt  ** (list-split-when-elt**   seq (car val) (cadr val)))

;;partition

(defmacro test-list--partition-test(command type)
  "
uses should-return t1"
  (declare (indent defun))
  `(if (eq should-return 'err)
       (should-error ,command)
     (let ((return ,command))
       (should (equal return should-return))
       ,(pcase-exhaustive type
          ('n   `(when return
                   ;;TODO: find a way to test all of return
                   (if (car return)
                       (should (eq t1 (car return)))
                     (should (eq t1 (cadr return))) )))
          ('*   `(let ((spot t1))
                   (while return
                     (let ((x (pop return)))
                       (if return
                           (should-not (eq spot x))
                         (should (eq spot x)) )
                       (setf spot (nthcdr (length x) spot)) ))))
          ('**  `(let ((spot t1))
                   (dolist (x return)
                     (should-not (eq spot x))
                     (setf spot (nthcdr (length x) spot)) )))
          ('nil `(cond
                  ((= (length return) 1)
                   (should (eq t1 (car return))) )
                  ((and (null (car return))
                        (= (length return) 2) )
                   (should (eq t1 (cadr return))) )
                  (t
                   (let ((spot t1))
                     (dolist (x return)
                       (should-not (eq spot x))
                       (setf spot (nthcdr (length x) spot)) )))))))))


(defmacro test-list-partition-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (index t1 should-return)
       ((0  '(a b c) nil)
        (1  '(a b c) '((a) (b) (c)))
        (2  '(a b c) '((a b) (c)))
        (3  '(a b c) '((a b c)))
        (4  '(a b c) '((a b c)))
        (2  '(a b c d e f g) '((a b) (c d) (e f) (g)))
        (3  '(a b c d e f g) '((a b c) (d e f) (g)))
        (-1 '(a b c) nil)
        (0  nil      nil)
        (1  nil      nil)
        (-1 nil      nil)
        (0  '(x)     nil)
        (1  '(x)     '((x)))
        (-1 '(x)     nil) )
       :copy-args  (t1)
       ,@(unless (eq type 'n)
           `(:const-args (t1)) )
       (test-list--partition-test (,fn t1 index) ,type) )))

(defmacro test-list-partition-when-cdr-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (pred t1 should-return)
       ((#'ignore                      '(a b c d e f g) '((a b c d e f g)))
        (#'identity                    '(a b c d e f g) '(nil (a) (b) (c) (d) (e) (f) (g)))
        ((lambda(x) (numberp (car x))) '(a 1 c 2 e 3 g) '((a) (1 c) (2 e) (3 g)))
        ((lambda(x) (numberp (car x))) '(1 b c 2 e 3 4) '(nil (1 b c) (2 e) (3) (4)))
        (#'error                       nil              nil)
        (#'error                       '(a)             'err)
        (#'ignore                      '(a)             '((a)))
        (#'identity                    '(a)             '(nil (a))) )
       :copy-args  (t1)
       ,@(unless (eq type 'n)
           `(:const-args (t1)) )
       (test-list--partition-test (,fn t1 pred) ,type) )))

(defmacro test-list-partition-when-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (pred t1 should-return)
       ((#'ignore   '(a b c d e f g) '((a b c d e f g)))
        (#'identity '(a b c d e f g) '(nil (a) (b) (c) (d) (e) (f) (g)))
        (#'numberp  '(a 1 c 2 e 3 g) '((a) (1 c) (2 e) (3 g)))
        (#'numberp  '(1 b c 2 e 3 4) '(nil (1 b c) (2 e) (3) (4)))
        (#'error    nil              nil)
        (#'error    '(a)             'err)
        (#'ignore   '(a)             '((a)))
        (#'identity '(a)             '(nil (a))) )
       :copy-args  (t1)
       ,@(unless (eq type 'n)
           `(:const-args (t1)) )
       (test-list--partition-test (,fn t1 pred) ,type) )))

(defmacro test-list-partition-when-elt-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (elt t1 should-return &optional testfn)
       (('x   '(a b c 1 e f g) '((a b c 1 e f g)))
        (1    '(1 1 1 1)       '(nil (1) (1) (1) (1)))
        (1    '(a 1 c 1 e 1 g) '((a) (1 c) (1 e) (1 g)))
        (1    '(1 b c 1 e 1 1) '(nil (1 b c) (1 e) (1) (1)))
        ('x   nil              nil            #'error)
        ('x   '(a)             'err           #'error)
        ('x   '(a)             '((a)))
        ('x   '(x)             '(nil (x)))
        ('(x) '(a (x) c)       '((a) ((x) c)))
        ('(x) '(a (x) c)       '((a (x) c))   #'eq) )
       :copy-args  (t1)
       ,@(unless (eq type 'n)
           `(:const-args (t1)) )
       (test-list--partition-test (,fn t1 elt testfn) ,type) )))

(test-list-partition-template list-npartition  n)
(test-list-partition-template list-partition*  *)
(test-list-partition-template list-partition** **)
;;(test-list-partition-template list-partition)

(test-list-partition-when-cdr-template list-npartition-when-cdr  n)
(test-list-partition-when-cdr-template list-partition-when-cdr*  *)
(test-list-partition-when-cdr-template list-partition-when-cdr** **)
;;(test-list-partition-when-cdr-template list-partition-when-cdr)

(test-list-partition-when-template list-npartition-when  n)
(test-list-partition-when-template list-partition-when*  *)
(test-list-partition-when-template list-partition-when** **)
;;(test-list-partition-when-template list-partition-when)

(test-list-partition-when-elt-template list-npartition-when-elt  n)
(test-list-partition-when-elt-template list-partition-when-elt*  *)
(test-list-partition-when-elt-template list-partition-when-elt** **)
;;(test-list-partition-when-elt-template list-partition-when-elt)


;;popn

(defmacro test-list--popn-test(command type index-command)
  "
uses should-return t1 should-after"
  (declare (indent defun))
  `(if (eq should-return 'err)
       (should-error ,command)
     (let* ((after  (nthcdr ,index-command t1))
            (als    t1)
            (return ,command))
       (should (equal return should-return))
       (should (equal t1     should-after))
       ,(pcase-exhaustive type
          ('n   `(progn
                   (when return
                     (should (eq return als)) )
                   (should (eq t1 after)) ))
          ('nil `(progn
                   (when return
                     (if t1
                         (should-not (eq return als))
                       (should (eq return als)) ))
                   (should (eq t1 after)) ))))))


(defmacro test-list-popn-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (n t1 should-return should-after)
       ((0  '(a b c) nil      '(a b c))
        (1  '(a b c) '(a)     '(b c))
        (2  '(a b c) '(a b)   '(c))
        (3  '(a b c) '(a b c) nil)
        (-1 '(a b c) nil      '(a b c))
        (4  '(a b c) '(a b c) nil)
        (0  nil      nil      nil)
        (-1 nil      nil      nil)
        (1  nil      nil      nil)
        (0  '(x)     nil      '(x))
        (-1 '(x)     nil      '(x))
        (1  '(x)     '(x)     nil) )
       :copy-args  (t1)
       (test-list--popn-test (,fn t1 n) ,type
         n ))))

(defmacro test-list-pop-until-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (pred t1 should-return should-after)
       ((#'identity '(a b c) nil      '(a b c))
        (#'numberp  '(1 b 3) nil      '(1 b 3))
        (#'numberp  '(a 2 c) '(a)     '(2 c))
        (#'numberp  '(a b 3) '(a b)   '(3))
        (#'ignore   '(a b c) '(a b c) nil)
        (#'error    nil      nil      nil)
        (#'identity '(x)     nil      '(x))
        (#'ignore   '(x)     '(x)     nil) )
       :copy-args  (t1)
       (test-list--popn-test (,fn t1 pred) ,type
         (or (seq-position (+test-clean-list t1)
                           nil
                           (lambda(x _)
                             (funcall pred x) ))
             (safe-length t1) )))))

(defmacro test-list-pop-until-after-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (n t1 should-return should-after)
       ((0  '(a b c) '(a)     '(b c))
        (1  '(a b c) '(a b)   '(c))
        (2  '(a b c) '(a b c) nil)
        (4  '(a b c) '(a b c) nil)
        (0  nil      nil      nil)
        (1  nil      nil      nil)
        (0  '(x)     '(x)     nil)
        (1  '(x)     '(x)     nil) )
       :copy-args  (t1)
       (test-list--popn-test (,fn t1 (nthcdr n t1)) ,type
         (1+ n) ))))

(defmacro test-list-pop-until-cdr-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (pred t1 should-return should-after)
       ((#'identity                   '(a b c) nil      '(a b c))
        ((lambda(x) (= (length x) 3)) '(a b c) nil      '(a b c))
        ((lambda(x) (= (length x) 2)) '(a b c) '(a)     '(b c))
        ((lambda(x) (= (length x) 1)) '(a b c) '(a b)   '(c))
        ((lambda(x) (= (length x) 0)) '(a b c) '(a b c) nil)
        (#'ignore                     '(a b c) '(a b c) nil)
        (#'error                      nil      nil      nil)
        (#'identity                   '(x)     nil      '(x))
        (#'ignore                     '(x)     '(x)     nil) )
       :copy-args  (t1)
       (test-list--popn-test (,fn t1 pred) ,type
         (let ((idx  0)
               (spot t1) )
           (while (and (consp spot)
                       (not (funcall pred spot)) )
             (pop spot)
             (setq idx (1+ idx)) )
           idx )))))

(defmacro test-list-pop-until-elt-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test (elt t1 should-return should-after &optional testfn)
       (('a   '(a b c)   'nil       '(a b c))
        ('b   '(a b c)   '(a)       '(b c))
        ('c   '(a b c)   '(a b)     '(c))
        ('x   '(a b c)   '(a b c)   nil)
        ('x   nil        'nil       nil      #'error)
        ('x   '(x)       'err       nil      #'error)
        ('x   '(x)       'nil       '(x))
        ('a   '(x)       '(x)       nil)
        ('(b) '(a (b) c) '(a)       '((b) c))
        ('(b) '(a (b) c) '(a (b) c) nil      #'eq) )
       :copy-args  (t1 elt)
       (test-list--popn-test (,fn t1 elt testfn) ,type
         (or (seq-position (+test-clean-list t1)
                           elt testfn )
             (safe-length t1) )))))



(test-list-popn-template list-npopn n)
(test-list-popn-template list-popn)

(test-list-pop-until-template list-npop-until n)
(test-list-pop-until-template list-pop-until)

(test-list-pop-until-after-template list-npop-until-after n)
(test-list-pop-until-after-template list-pop-until-after)

(test-list-pop-until-cdr-template list-npop-until-cdr n)
(test-list-pop-until-cdr-template list-pop-until-cdr)

(test-list-pop-until-elt-template list-npop-until-elt n)
(test-list-pop-until-elt-template list-pop-until-elt)



(provide 'test-list-split)
;;(provide 'test-list)
;;; test-list-split.el ends here
