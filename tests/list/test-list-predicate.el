;;; test-list-predicate.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: May 11, 2022
;; Modified: May 11, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/test-list-predicate
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'ert)
(require '+test)
(require 'list/predicate)


(ert-deftest test-list-clean-p()
  (+test-test(should-return t1)
    ((t   '(1 2 3))
     (t   '(1 2 (3 4)))
     (t   '(1 2 nil))
     (t   '(1))
     (t   '(nil))
     (t   nil)
     (nil 'x)
     (nil '(1 2 . 3))
     (nil '(1 . 3))
     (nil '((1 2) . 3))
     (nil '[1 2])
     (nil '[(1 2 3)])
     (nil '[])
     (nil "test") )
    :copy-args  (t1)
    :const-args (t1)
    (should (equal (list-clean-p t1) should-return)) )
  (when +test-loops
    (with-timeout (2 (error "Timeout running loop test"))
      (let ((t1 (list 1 2 3)))
        (setf (nthcdr 3 t1) t1)
        (should (not (list-clean-p t1))) ))))

(ert-deftest test-list-clean-fast-p()
  (+test-test(should-return t1)
    ((t   '(1 2 3))
     (t   '(1 2 (3 4)))
     (t   '(1 2 nil))
     (t   '(1))
     (t   '(nil))
     (t   nil)
     (nil 'x)
     (nil '(1 2 . 3))
     (nil '(1 . 3))
     (nil '((1 2) . 3))
     (nil '[1 2])
     (nil '[(1 2 3)])
     (nil '[])
     (nil "test") )
    :copy-args  (t1)
    :const-args (t1)
    (should (equal (list-clean-fast-p t1) should-return)) ))

;;any-p

(ert-deftest test-list-any-p()
  (+test-test(should-return t1 pred)
    ((nil  '(a b c)   #'numberp)
     (t    '(1 b c)   #'numberp)
     (t    '(a 2 c)   #'numberp)
     (t    '(a b 3)   #'numberp)
     (t    '(1 2 3)   #'numberp)
     (nil  '(1 2 3)   #'ignore)
     ('err 'x         #'ignore)
     ('err 'x         #'identity)
     (nil  nil        #'error)
     ('err '(a b . c) #'numberp)
     (t    '(a 2 . c) #'numberp) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-any-p t1 pred))
      (if should-return
          (should (list-any-p t1 pred))
        (should-not (list-any-p t1 pred)) ))))

(ert-deftest test-list-every-p()
  (+test-test(should-return t1 pred)
    ((nil  '(a b c)   #'numberp)
     (nil  '(1 b c)   #'numberp)
     (nil  '(1 b 3)   #'numberp)
     (nil  '(a 2 3)   #'numberp)
     (t    '(1 2 3)   #'numberp)
     ('err 'x         #'ignore)
     ('err 'x         #'identity)
     ('t   nil        #'error)
     (nil  '(1 b . c) #'numberp)
     ('err '(1 2 . c) #'numberp) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-every-p t1 pred))
      (if should-return
          (should (list-every-p t1 pred))
        (should-not (list-every-p t1 pred)) ))))

(ert-deftest test-list-contains-p()
  (+test-test(should-return val t1 &optional func)
    ((t    'a  '(a b c))
     (t    'b  '(a b c))
     (t    'c  '(a b c))
     (nil  'x  '(a b c))
     (nil  nil '(a b c))
     (t    nil '(nil b c))
     (t    'b  '(nil b c))
     (nil  'x  '(nil b c))
     (t    nil '(a nil c))
     (t    'a  '(a nil c))
     (t    'c  '(a nil c))
     (nil  'x  '(a nil c))
     (t    nil '(a b nil))
     (t    'b  '(a b nil))
     (nil  'x  '(a b nil))
     (nil  'x  '(nil))
     (t    nil '(nil))
     (t    'a  '(a))
     (nil  nil '(a))
     ('err nil 'a)
     ('err nil '[a b c])
     ('err nil '[])
     ('err nil '(a . b))
     ('err 'b  '(a . b))
     (t    'a  '(a . b))
     (nil  [1] '(a b [1]) #'eq)
     (t    [1] '(a b [1])) )
    :copy-args  (t1)
    :const-args (t1)
    (if (eq should-return 'err)
        (should-error (list-contains-p t1 val func))
      (should (equal (list-contains-p t1 val func) should-return)) )))

;;length

(defmacro test-list-length-p-template(fn type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test(len t1 &optional err)
       ((3  '(1 2 3))
        (2  '(1 2 3))
        (0  '(1 2 3))
        (-1 '(1 2 3))
        (4  '(1 2 3))
        (1  '(1))
        (0  '(1))
        (-1 '(1))
        (2  '(1))
        (0  nil)
        (-1 nil)
        (1  nil)
        (1  '(a b . c))
        (2  '(a b . c))
        (3  '(a b . c) 'err)
        (-1 'x)
        (0  'x)
        (1  'x         'err) )
       :copy-args  (t1)
       :const-args (t1)
       (if err
           (should-error (,fn t1 len))
         (if (,type (let ((l (safe-length t1)))
                      (if (listp (nthcdr l t1))
                          l
                        (+ l 1) ))
                    len )
             (should (,fn t1 len))
           (should-not (,fn t1 len)) )))))

(test-list-length-p-template list-length=  =)
(test-list-length-p-template list-length<  <)
(test-list-length-p-template list-length<= <=)
(test-list-length-p-template list-length>  >)
(test-list-length-p-template list-length>= >=)

;;length*

(defmacro test-list-length-p*-template(fn type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn)))()
     (+test-test(t1 t2 &optional err)
       (('(1 2 3)   '(a b c))
        ('(1 2 3)   '(a b c d))
        ('(1 2 3)   '(a b))
        ('(1 2 3)   '(a))
        ('(1 2 3)   nil)
        ('(1)       '(a))
        ('(1)       '(a b))
        ('(1)       nil)
        (nil        nil)
        (nil        '(a))
        ('(a b . c) '(a b . c) 'err)
        ('(a b . c) '(a b))
        ('(a b . c) '(a))
        ('(a b . c) '(a b c)   'err)
        ('(a b . c) '(a b c d) 'err)
        ('x         nil)
        ('x         'a         'err)
        ('x         '(a)       'err)
        ('(x)       'a         'err) )
       :copy-args  (t1 t2)
       :const-args (t1 t2)
       (if err
           (should-error (,fn t1 t2))
         (if (,type (let ((l (safe-length t1)))
                      (if (listp (nthcdr l t1))
                          l
                        (+ l 1) ))
                    (let ((l (safe-length t2)))
                      (if (listp (nthcdr l t2))
                          l
                        (+ l 1) )) )
             (should (,fn t1 t2))
           (should-not (,fn t1 t2)) )))))

(test-list-length-p*-template list-length=*  =)
(test-list-length-p*-template list-length<*  <)
(test-list-length-p*-template list-length<=* <=)
(test-list-length-p*-template list-length>*  >)
(test-list-length-p*-template list-length>=* >=)


(provide 'test-list-predicate)
;;(provide 'test-list)
;;; test-list-predicate.el ends here
