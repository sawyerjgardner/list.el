;;; test-list-drop.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 23, 2022
;; Modified: April 23, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/test-list-drop
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


(require 'ert)
(require '+test)
(require 'split-template)
(require 'list/drop)

(defmacro test-list-drop-template(type destructive command)
  ""
  (declare (indent defun))
  `(ert-deftest ,(intern (concat "test-" (symbol-name (car command))))()
     (+test-split-template
       :index-type ,type
       :seq-type   list
       :const      t
       :dotted     ,(or destructive 'error )
       (ignore before)
       (if (eq after 'err)
           (should-error ,command)
         (let ((return ,command))
           (should (equal return after))
           ,(if destructive
                `(should (eq return (nthcdr index seq)))
              `(when return
                 (should-not (eq return (nthcdr index seq))) )))))))

(test-list-drop-template at   t   (list-ndrop-until-after seq (nthcdr val seq)))
(test-list-drop-template at   nil (list-drop-until-after  seq (nthcdr val seq)))

(test-list-drop-template int  t   (list-ndrop           seq val))
(test-list-drop-template pred t   (list-ndrop-until     seq val))
(test-list-drop-template elt  t   (list-ndrop-until-elt seq (car val) (cadr val)))
(test-list-drop-template cdr  t   (list-ndrop-until-cdr seq val))

(test-list-drop-template int  nil (list-drop           seq val))
(test-list-drop-template pred nil (list-drop-until     seq val))
(test-list-drop-template elt  nil (list-drop-until-elt seq (car val) (cadr val)))
(test-list-drop-template cdr  nil (list-drop-until-cdr seq val))

;;drop setf

(defmacro test-list--drop-setf-test(command type index-command)
  "
uses should-return val t1"
  (declare (indent defun))
  `(if (eq should-return 'err)
       (should-error (setf ,command val))
     (let ((index ,index-command)
           (als   t1)
           (len   (safe-length t1)) )
       (should (eq    (setf ,command val) val))
       (should (equal t1 should-return))
       ,@(pcase-exhaustive type
           ('n   `((ignore len)
                   (should (eq val (nthcdr index t1)))
                   (when (> index 0)
                     (should (eq t1 als)) )))
           ('nil `((ignore len)
                   (when t1
                     (should-not (eq val (nthcdr index t1)))
                     (should-not (eq t1  als)) )))))) )

;;TODO: could use more tests
(defmacro test-list-drop-until-after-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(n t1 val should-return)
       ((0  '(1 2 3) '(a b) '(1 a b))
        (1  '(1 2 3) '(a b) '(1 2 a b))
        (2  '(1 2 3) '(a b) '(1 2 3 a b))
        (4  '(1 2 3) '(a b) 'err)
        ,@(pcase-exhaustive type
            ('n
             `((0 '(1 2) 'x '(1 . x))
               (1 '(1 2) 'x '(1 2 . x)) ))
            ('nil
             `((0 '(1 2) 'x 'err)
               (1 '(1 2) 'x 'err) ))))
       :copy-args  (n t1 val)
       :const-args (n    val)
       (test-list--drop-setf-test (,fn t1 (nthcdr n t1)) ,type
         (1+ n) ))))

(defmacro test-list-drop-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(n t1 val should-return)
       ((0  '(1 2 3) '(a b) '(a b))
        (1  '(1 2 3) '(a b) '(1 a b))
        (2  '(1 2 3) '(a b) '(1 2 a b))
        (3  '(1 2 3) '(a b) '(1 2 3 a b))
        (4  '(1 2 3) '(a b) 'err)
        (-1 '(1 2 3) '(a b) 'err)
        (0  'x       '(1)   '(1))
        (1  'x       '(1)   'err)
        ,@(pcase-exhaustive type
            ('n
             `((0 '(1 2) 'x 'x)
               (1 '(1 2) 'x '(1 . x)) ))
            ('nil
             `((0 '(1 2) 'x 'err)
               (1 '(1 2) 'x 'err) ))))
       :copy-args  (n t1 val)
       :const-args (n    val)
       (test-list--drop-setf-test (,fn t1 n) ,type
         n ))))

(defmacro test-list-drop-until-cdr-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(pred t1 val should-return)
       ((#'ignore                     '(a b c) '(x y) '(a b c x y))
        (#'identity                   '(a b c) '(x y) '(x y))
        (#'error                      nil      '(x y) '(x y))
        ((lambda(x) (= (length x) 2)) '(a b c) '(x y) '(a x y))
        ((lambda(x) (= (length x) 1)) '(a b c) '(x y) '(a b x y))
        (#'ignore                     '(a)     '(x y) '(a x y))
        (#'identity                   '(a)     '(x y) '(x y))
        (#'error                      nil      '(x y) '(x y))
        ,@(pcase-exhaustive type
            ('n
             `((#'ignore                  nil          'x   'x)
               (#'identity                '(a b)       'x   'x)
               (#'ignore                  '(a b . c)   'x   'err)
               ((lambda(x) (not (car x))) '(a nil . c) 'x   '(a . x))
               ((lambda(x) (not (car x))) '(a nil . c) '(x) '(a x)) ))
            ('nil
             `((#'ignore                  nil          'x   'err)
               (#'identity                '(a b)       'x   'err)
               (#'ignore                  '(a b . c)   'x   'err)
               ((lambda(x) (not (car x))) '(a nil . c) 'x   'err)
               ((lambda(x) (not (car x))) '(a nil . c) '(x) '(a x)) ))))
       :copy-args  (pred t1 val)
       :const-args (pred    val)
       (test-list--drop-setf-test (,fn t1 pred) ,type
         (let ((idx  0)
               (spot t1) )
           (while (and (consp spot)
                       (not (funcall pred spot)) )
             (list-pop spot)
             (setq idx (1+ idx)) )
           idx )))))

(defmacro test-list-drop-until-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(pred t1 val should-return)
       ((#'numberp  '(1 b c) '(x y) '(x y))
        (#'numberp  '(a 2 c) '(x y) '(a x y))
        (#'numberp  '(a b 3) '(x y) '(a b x y))
        (#'numberp  '(a b c) '(x y) '(a b c x y))
        (#'not      '(a)     '(x y) '(a x y))
        (#'not      '(nil)   '(x y) '(x y))
        (#'error    nil      '(x y) '(x y))
        (#'ignore   'x       nil    'err)
        ,@(pcase-exhaustive type
            ('n
             `((#'ignore  nil        'x 'x)
               (#'ignore  '(a b . c) 'x 'err)
               (#'numberp '(a 1 . c) 'x '(a . x)) ))
            ('nil
             `((#'ignore  nil        'x 'err)
               (#'ignore  '(a b . c) 'x 'err)
               (#'numberp '(a 1 . c) 'x 'err) ))))
       :copy-args  (pred t1 val)
       :const-args (pred    val)
       (test-list--drop-setf-test (,fn t1 pred) ,type
         (or (seq-position (+test-clean-list t1)
                           nil
                           (lambda(x _)
                             (funcall pred x) ))
             (safe-length t1) )))))

(defmacro test-list-drop-until-elt-setf-template(fn &optional type)
  ""
  `(ert-deftest ,(intern (concat "test-" (symbol-name fn) "-setf"))()
     (+test-test(elt t1 val should-return &optional testfn)
       (('a   '(a b c)     '(x y) '(x y))
        ('b   '(a b c)     '(x y) '(a x y))
        ('c   '(a b c)     '(x y) '(a b x y))
        ('x   '(a b c)     '(x y) '(a b c x y))
        (nil  '(a nil c)   '(x y) '(a x y))
        ('b   '(nil b nil) '(x y) '(nil x y))
        ('a   '(a)         '(x y) '(x y))
        ('x   '(a)         '(x y) '(a x y))
        ('(x) '(a (x) c)   '(x y) '(a x y))
        ('(x) '(a (x) c)   '(x y) '(a (x) c x y) #'eq)
        ('x   nil          '(x y) '(x y)         #'error)
        ,@(pcase-exhaustive type
            ('n
             `(('x    nil        'x   'x       #'error)
               ('x    '(a b . c) 'x   'err)
               ('x    '(a x . c) 'y   '(a . y))
               ('x    '(a x . c) '(y) '(a y)) ))
            ('nil
             `(('x    nil        'x   'err)
               ('x    '(a b . c) 'x   'err)
               ('x    '(a x . c) 'y   'err)
               ('x    '(a x . c) '(y) '(a y)) ))))
       :copy-args  (elt t1 val testfn)
       :const-args (elt    val testfn)
       (test-list--drop-setf-test (,fn t1 elt testfn) ,type
         (or (seq-position (+test-clean-list t1)
                           elt testfn )
             (safe-length t1) )))))


(test-list-drop-until-after-setf-template list-ndrop-until-after n)
(test-list-drop-until-after-setf-template list-drop-until-after)

(test-list-drop-setf-template list-ndrop n)
(test-list-drop-setf-template list-drop)

(test-list-drop-until-cdr-setf-template list-ndrop-until-cdr n)
(test-list-drop-until-cdr-setf-template list-drop-until-cdr)

(test-list-drop-until-setf-template list-ndrop-until n)
(test-list-drop-until-setf-template list-drop-until)

(test-list-drop-until-elt-setf-template list-ndrop-until-elt n)
(test-list-drop-until-elt-setf-template list-drop-until-elt)


(provide 'test-list-drop)
;; (provide 'test-list)
;;; test-list-drop.el ends here
