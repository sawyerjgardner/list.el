;;; predicate.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: May 10, 2022
;; Modified: May 10, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/predicate
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'list/simple)

;;TODO: set-equal-p

;; TODO: find better name
(defun list-clean-p(list)
  "Return t if LIST is a clean list.
a list is considered clean if it is nil terminated."
  (declare (pure t)
           (side-effect-free error-free) )
  (ignore-errors
    (let ((p1 list)
          (p2 list) )
      (while (setq p1 (cdr  p1)
                   p2 (cddr p2) )
        (when (eq p1 p2)
          (signal 'error nil) ))
      t )))

(defun list-clean-fast-p(list)
  "Return t if LIST is a clean list.
a list is considered clean if it is nil terminated.
if LIST ends in a cycle, then this function will not return"
  (declare (pure t)
           (side-effect-free error-free) )
  (while (consp list)
    (list-pop list) )
  (not list) )

;;contains

(defun list-any-p(list pred)
  ""
  (while (and list
              (not (funcall pred (car list))) )
    (list-pop list) )
  (and list t) )

(defun list-every-p(list pred)
  ""
  (while (and list
              (funcall pred (car list)) )
    (list-pop list) )
  (not list) )

(defun list-contains-p(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (while (and list
              (not (funcall testfn (car list) elt)) )
    (list-pop list) )
  (and list t) )

;;length

(defun list-empty-p(list)
  ""
  (null list) )

(defun list-length=(list length)
  ""
  (while (and list
              (> length 0) )
    (setf length (1- length)
          list   (cdr list) ))
  (and (null list)
       (= length 0)
       t ))

(defun list-length<(list length)
  ""
  (while (and list
              (> length 0) )
    (setf length (1- length)
          list   (cdr list) ))
  (and (null list)
       (> length 0)
       t) )

(defun list-length<=(list length)
  ""
  (while (and list
              (> length 0) )
    (setf length (1- length)
          list   (cdr list) ))
  (and (null list)
       (>= length 0)
       t ))

(defun list-length>(list length)
  ""
  (not (list-length<= list length)))

(defun list-length>=(list length)
  ""
  (not (list-length< list length)) )

;;length*

;;TODO: work with any number of arguments
(defun list-length=*(list1 list2)
  ""
  (while (and list1 list2)
    (list-pop list1)
    (list-pop list2) )
  (and (null list1) (null list2)) )

(defun list-length<*(list1 list2)
  ""
  (while (and list1 list2)
    (list-pop list1)
    (list-pop list2) )
  (and list2 (null list1)) )

(defun list-length<=*(list1 list2)
  ""
  (while (and list1 list2)
    (list-pop list1)
    (list-pop list2) )
  (null list1) )

(defun list-length>*(list1 list2)
  ""
  (while (and list1 list2)
    (list-pop list1)
    (list-pop list2) )
  (and list1 (null list2)) )

(defun list-length>=*(list1 list2)
  ""
  (while (and list1 list2)
    (list-pop list1)
    (list-pop list2) )
  (null list2) )

;;assert

(defun list--assert-clean(list)
  "Signal wrong type if LIST dose not pass `list-clean-p'."
  (declare (pure t))
  (unless (list-clean-p list)
    (signal 'wrong-type-argument (list 'list-clean-p list)) ))

(provide 'list/predicate)
;;(provide 'list)
;;; predicate.el ends here
