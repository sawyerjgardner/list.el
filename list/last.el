;;; last.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 14, 2022
;; Modified: April 14, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/last
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'macroexp)
(require 'list-tools)
(require 'list/simple)
(require 'list/drop)


;;last

(defun list-tail(list)
  "Return the last cdr in LIST.
becouse this only accepts nil-terminated lists, it will always returns nil."
  (declare (pure t)
           (side-effect-free t) )
  ;;NOTE: this line is just to throw errors
  (while (setf list (cdr list)))
  nil )

(defun list-last1(list)
  "Return the last link of LIST. Its car is the last element.
If LIST is nil, return nil."
  (declare (pure t)
           (side-effect-free t) )
  (list-tools--while-let ((next (cdr list)))
    (setf list next) )
  list )

(defun list-last(list &optional n)
  "Return the last N links of LIST.
If LIST is nil, return nil.
If N is nil or missing, return the last link of LIST.
If N is bigger than the length of LIST, return LIST."
  (declare (pure t)
           (side-effect-free t)
           (compiler-macro (lambda(original)
                             (list-tools--if-let-constant ((idx n))
                                 (pcase idx
                                   (0    `(list-tail  ,list))
                                   (1    `(list-last1 ,list))
                                   ('nil `(list-last1 ,list))
                                   (_    original) )
                               original ))))
  (let ((spot (nthcdr (or n 1) list)))
    (while spot
      (list-pop list)
      (list-pop spot) )
    list ))

(list-tools--gv-define-c-setter list-tail(val got)
  ;;TODO: this line might not be needed.
  (macroexp-let2 macroexp-copyable-p v val
    (let ((spot (make-symbol "spot")))
      `(if (null ,got)
           (setq ,got ,v)
         (let ((,spot (list-last1 ,got)))
           (setf (cdr ,spot) ,v) )))))

(gv-define-expander list-last1
  (lambda(do sub-place)
    (let ((end  (make-symbol "end"))
          (spot (make-symbol "spot"))
          (next (make-symbol "next")) )
      (list-tools--gv-letplace-simple sub-got sub-place
        `(let* ((,end  ,sub-got)
                (,spot nil) )
           (list-tools--while-let ((,next (cdr ,end)))
             (setf ,spot ,end
                   ,end  ,next ))
           ,(funcall do end
                     (lambda(val)
                       ;;TODO: this line might not be needed.
                       (macroexp-let2 macroexp-copyable-p v val
                         `(if ,spot
                              (setf (cdr ,spot) ,v)
                            (setf ,sub-got ,v) )))))))))

(gv-define-expander list-last
  (lambda(do sub-place &optional n)
    (pcase (list-tools--if-let-constant ((idx n))
               idx
             'unknown)
      (0    (gv-get `(list-tail  ,sub-place) do))
      (1    (gv-get `(list-last1 ,sub-place) do))
      ('nil (gv-get `(list-last1 ,sub-place) do))
      (_    (let ((end    (make-symbol "end"))
                  (spot   (make-symbol "spot")) )
              (list-tools--gv-letplace-simple sub-got sub-place
                `(let* ((,end  (nthcdr (or ,n 1) ,sub-got))
                        (,spot nil) )
                   (when ,end
                     (setf ,spot ,sub-got)
                     (list-pop ,end)
                     (while ,end
                       (list-pop ,spot)
                       (list-pop ,end) ))
                   ,(funcall do `(if ,spot ;;TODO: optimise getter
                                     (cdr ,spot)
                                   ,sub-got )
                             (lambda(val)
                               `(if ,spot
                                    (setf (cdr ,spot) ,val)
                                  (setf ,sub-got ,val) ))))))))))

;;last before cdr

(defun list--last-before-cdr1-skip(list pred)
  ""
  (let ((end (cdr list)))
    (while (and end
                (not (funcall pred end)) )
      (setf list end
            end  (cdr end) )))
  list )

(defun list-last-before-cdr1(list pred)
  ""
  (if (and list
           (not (funcall pred list)) )
      (list--last-before-cdr1-skip list pred)
    list ))

(defun list-last-before-cdr(list pred &optional n)
  ""
  (declare (compiler-macro (lambda(original)
                             (list-tools--if-let-constant ((idx n))
                                 (pcase idx
                                   (0    `(list-ndrop-until-cdr  ,list ,pred))
                                   (1    `(list-last-before-cdr1 ,list ,pred))
                                   ('nil `(list-last-before-cdr1 ,list ,pred))
                                   (_    original) )
                               original ))))
  (setf n (or n 1))
  (let ((end list))
    (while (and end
                (> n 0) )
      (setf n (1- n))
      (setf end (unless (funcall pred end)
                  (cdr end) )))
    (while (and end
                (not (funcall pred end)))
      (list-pop end)
      (list-pop list) )
    list ))

;;TODO: optimize
(gv-define-expander list-last-before-cdr1
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (macroexp-let2* ignore ((spot nil)
                                (end  sub-got) )
          `(progn
             (when (and ,end
                        (not (funcall ,fn ,end)) )
               (while (and (cdr ,end)
                           (not (funcall ,fn (cdr ,end))) )
                 (setf ,spot ,end
                       ,end  (cdr ,end) )))
             ,(funcall do end
                       (lambda(val)
                         `(if ,spot
                              (setf (cdr ,spot) ,val)
                            (setf ,sub-got ,val) )))))))))

(gv-define-expander list-last-before-cdr
  (lambda(do sub-place pred &optional n)
    (pcase (list-tools--if-let-constant ((idx n))
               idx
             'unknown)
      (0    (gv-get `(list-ndrop-until-cdr  ,sub-place ,pred) do))
      (1    (gv-get `(list-last-before-cdr1 ,sub-place ,pred) do))
      ('nil (gv-get `(list-last-before-cdr1 ,sub-place ,pred) do))
      (_    (list-tools--gv-letplace-simple sub-got sub-place
              (macroexp-let2 macroexp-copyable-p fn pred
                (macroexp-let2* ignore ((idx  `(or ,n 1))
                                        (spot nil)
                                        (end  sub-got) )
                  `(progn
                     (while (and ,end
                                 (not (funcall ,fn (list-pop-link ,end)))
                                 (>= (setf ,idx (1- ,idx)) 0) ))
                     (when (< ,idx 0)
                       (setf ,spot ,sub-got)
                       (when ,end
                         (while (unless (funcall ,fn ,end)
                                  (list-pop ,spot)
                                  (setf ,end (cdr ,end)) ))))
                     ,(funcall do `(if ,spot ;;TODO: optimize getter
                                       (cdr ,spot)
                                     ,sub-got )
                               (lambda(val)
                                 `(if ,spot
                                      (setf (cdr ,spot) ,val)
                                    (setf ,sub-got ,val) )))))))))))


;;last before

(defun list-last-before1(list pred)
  ""
  (list-last-before-cdr1 list (lambda(x) (funcall pred (car x)))))

(defun list-last-before(list pred &optional n)
  ""
  (declare (compiler-macro
            (lambda(original)
              (list-tools--if-let-constant ((idx n))
                  (pcase idx
                    (0    `(list-ndrop-until  ,list ,pred))
                    (1    `(list-last-before1 ,list ,pred))
                    ('nil `(list-last-before1 ,list ,pred))
                    (_    original) )
                original ))))
  (list-last-before-cdr list (lambda(x) (funcall pred (car x))) n))

(gv-define-expander list-last-before1
  (lambda(do sub-place pred)
    (gv-get `(list-last-before ,sub-place ,pred 1) do)))

(gv-define-expander list-last-before
  (lambda(do sub-place pred &optional n)
    (let ((x  (make-symbol "x")))
      (list-tools--gv-letplace-simple sub-got sub-place
        (macroexp-let2* macroexp-copyable-p ((fn  pred)
                                             (idx n) )
          (gv-get `(list-last-before-cdr ,sub-got
                                         (lambda(,x) (funcall ,fn (car ,x)))
                                         ,idx )
                  do ))))))

;;last before elt

(defun list-last-before-elt1(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-last-before list (lambda(x) (funcall testfn x elt)) 1) )

(defun list-last-before-elt(list elt &optional n testfn)
  ""
  (declare (compiler-macro
            (lambda(original)
              (list-tools--if-let-constant ((idx n))
                  (pcase idx
                    (0    `(list-ndrop-until-elt  ,list ,elt ,testfn))
                    (1    `(list-last-before-elt1 ,list ,elt ,testfn))
                    ('nil `(list-last-before-elt1 ,list ,elt ,testfn))
                    (_    original) )
                original ))))
  (setf testfn (or testfn #'equal))
  (list-last-before list (lambda(x) (funcall testfn x elt)) n) )

(gv-define-expander list-last-before-elt1
  (lambda(do sub-place elt &optional testfn)
    (gv-get `(list-last-before-elt ,sub-place ,elt 1 ,testfn) do)))

(gv-define-expander list-last-before-elt
  (lambda(do sub-place elt &optional n testfn)
    (let ((x  (make-symbol "x")))
      (list-tools--gv-letplace-simple sub-got sub-place
        (macroexp-let2* macroexp-copyable-p ((e   elt)
                                             (idx n)
                                             (tfn `(or ,testfn #'equal)) )
          (gv-get `(list-last-before ,sub-got
                                     (lambda(,x) (funcall ,tfn ,x ,e))
                                     ,idx )
                  do ))))))

(provide 'list/last)
;; (provide 'list)
;;; last.el ends here
