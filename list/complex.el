;;; complex.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 05, 2022
;; Modified: April 05, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/complex
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'subr-x)
(require 'gv)
(require 'list-tools)
(require 'list/simple)
(require 'list/last)
(require 'list/build)
(require 'list/loop)
(require 'list/predicate)

;;TODO: split
;;TODO: partition
;;TODO: group-by
;;TODO: rotate
;;TODO: uniq
;;TODO: union
;;TODO: intersection
;;TODO: difference
;;TODO: filter
;;TODO: reduce
;;TODO: some
;;TODO: sort
;;TODO: position
;;TODO: sub-list
;;TODO: mapcat
;;TODO: random

;;find

(defun list-find(list pred &optional default)
  "Return the first element in LIST for which (PRED element) is non-nil.
If no element is found, return DEFAULT."
  (if-let ((out (list-last-before list pred 0)))
      (car out)
    default ))

(defun list-find-set(list pred val)
  ""
  (if-let ((out (list-last-before list pred 0)))
      (setf (car out) val) ))

(gv-define-expander list-find
  (lambda(do sub-place pred &optional default-place)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p spot `(list-last-before ,sub-got ,pred 0)
        (gv-get `(if ,spot
                     (car ,spot)
                   ,(or default-place
                        (macroexp-let2 ignore tmp nil
                          tmp )))
                do )))))

(defun list-find-elt(list elt &optional testfn default)
  "Return the first element in LIST that matches ELT.
TESTFN is used to test equality, defaults to `eq'.
If no element is found, return DEFAULT.
if a value is being asighned to list-find and no element is found, the asighned
value is passed to the function DEFAULT."
  (if-let ((out (list-last-before-elt list elt 0 testfn)))
      (car out)
    default ))

(defun list-find-elt-set(list elt testfn val)
  ""
  (when-let ((out (list-last-before-elt list elt 0 testfn)))
    (setf (car out) val) ))

(gv-define-expander list-find-elt
  (lambda(do sub-place elt &optional testfn default-place)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p spot `(list-last-before-elt ,sub-got ,elt 0 ,testfn)
        (gv-get `(if ,spot
                     (car ,spot)
                   ,(or default-place
                        (macroexp-let2 ignore tmp nil
                          tmp )))
                do )))))

;;count

(defun list-count(list pred)
  ""
  (let ((out 0))
    (while (setf list (list-ndrop-until list pred))
      (setf out  (1+ out)
            list (cdr list) ))
    out ))

(defun list-count-cdr(list pred)
  ""
  (let ((out 0))
    (while (setf list (list-ndrop-until-cdr list pred))
      (setf out  (1+ out)
            list (cdr list) ))
    out ))

(defun list-count-elt(list elt &optional testfn)
  ""
  (let ((out 0))
    (while (setf list (list-ndrop-until-elt list elt testfn))
      (setf out  (1+ out)
            list (cdr list) ))
    out ))

;;filter

(defun list-filter(list pred)
  ""
  (list-build-while (setf list (list-last-before list pred 0))
    (list-pop list) ))

(defun list-nfilter(list pred)
  ""
  (let* ((out  (list-last-before list pred 0))
         (spot out) )
    (while (cdr spot)
      (setf (cdr spot) (list-last-before (cdr spot) pred 0)
            spot       (cdr spot) ))
    out ))

(defmacro list-nfilter-place(place pred)
  ""
  (gv-letplace (getter setter) place
    (funcall setter `(list-nfilter ,getter ,pred)) ))

;;TODO: filter setf

(defun list-filter-elt(list elt &optional testfn)
  ""
  (list-build-while (setf list (list-last-before-elt list elt 0 testfn))
    (list-pop list) ))

(defun list-nfilter-elt(list elt &optional testfn)
  ""
  (let* ((out  (list-last-before-elt list elt 0 testfn))
         (spot out) )
    (while (cdr spot)
      (setf (cdr spot) (list-last-before-elt (cdr spot) elt 0 testfn)
            spot       (cdr spot) ))
    out ))

(defmacro list-nfilter-elt-place(place elt &optional testfn)
  ""
  (gv-letplace (getter setter) place
    (funcall setter `(list-nfilter-elt ,getter ,elt ,testfn)) ))

;;TODO: filter elt setf

;;insert

;;TODO: do a single pass instead of the possible 2
(defmacro list-insert-setf(place element idx)
  "Insert ELEMENT into the list in PLACE at index IDX."
  (gv-letplace (getter setter) place
    (macroexp-let2* macroexp-copyable-p ((got getter)
                                         (val element)
                                         (at idx) )
      (let* ((spot (make-symbol "spot")))
        `(progn
           (if (>  1 ,at)
               (list-push ,val ,got)
             (if-let (,spot (nthcdr (1- ,at) ,got))
                 (setf (cdr ,spot) (cons ,val (cdr ,spot)))
               (setf (list-tail ,got) (cons ,val nil)) ))
           ,(funcall setter got) )))))

(defmacro list-add-setf(place element &optional compare-fn)
  "Add ELEMENT to the list in PLACE if not already present.
The test for ELEMENT is done with COMPARE-FN (defaults to `eq')."
  (declare (debug (gv-place form)))
  (gv-letplace (getter setter) place
    (macroexp-let2* macroexp-copyable-p ((got getter)
                                         (val element) )
      `(if (list-contains-p ,got ,val ,compare-fn)
           ,got
         ,(funcall setter `(cons ,val ,got)) ))))

;;TODO: optimize
(defmacro list-add-at-setf(place element idx &optional compare-fn)
  "Insert ELEMENT into the list in PLACE at index IDX if its not already present.
The test for ELEMENT is done with COMPARE-FN (defaults to `eq')."
  (declare (debug (gv-place form)))
  (gv-letplace (getter setter) place
    (let ((got (make-symbol "got")))
      `(let ((,got ,getter))
         ,(macroexp-let2* macroexp-copyable-p ((val element))
            `(if (list-contains-p ,got ,val ,compare-fn)
                 ,got
               (list-insert-setf ,got ,val ,idx)
               ,(funcall setter got) ))))))

;;shorten

(defmacro list-shorten-setf(place length)
  "Shorten the length of the list in PLACE to LENGTH.
this dose nothing if LENGTH is greater then the length of the list."
  (gv-letplace (getter setter) place
    (let ((got (make-symbol "got"))
          (idx (make-symbol "idx"))
          (end (make-symbol "end")) )
      `(let ((,got ,getter)
             (,idx ,length) )
         (if (> ,idx 0)
             (when-let (,end (nthcdr (1- ,idx) ,got))
               (setf (cdr ,end) nil) )
           (setq ,got nil) )
         ,(funcall setter got) ))))

(provide 'list/complex)
;; (provide 'list)
;;; complex.el ends here
