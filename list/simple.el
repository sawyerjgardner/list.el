;;; simple.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 05, 2022
;; Modified: April 05, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/simple
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


(require 'subr-x)
(require 'gv)
(require 'macroexp)
(require 'list-tools)

;;push pop

(defmacro list-push(newelt place)
  "Add NEWELT to the list stored in the generalized variable PLACE.
This is morally equivalent to (setf PLACE (cons NEWELT PLACE)),
except that PLACE is evaluated only once (after NEWELT)."
  (declare (debug (form gv-place)))
  (macroexp-let2 macroexp-copyable-p x newelt
    (gv-letplace (getter setter) place
      (funcall setter `(cons ,x ,getter)) )))

(defmacro list-pop(place)
  "Return the first element of PLACE's value, and remove it from the list.
PLACE must be a generalized variable whose value is a list.
If the value is nil, `list-pop' returns nil but does not actually
change the list."
  (declare (debug (gv-place)))
  (list-tools--gv-letplace-let (got setter) place
    `(car-safe (prog1 ,got
                 ,(funcall setter `(cdr ,got)) ))))


(defmacro list-push-link(newlink place)
  "Add NEWLINK to the begining of the list stored in PLACE.
the cdr of NEWLINK will be set to the old value of PLACE.
This is morally equivalent to (setf (cdr NEWELT) PLACE PLACE NEWELT)),
except that PLACE is evaluated only once (after NEWELT)."
  (declare (debug (form gv-place)))
  (macroexp-let2 ignore x newlink
    (gv-letplace (getter setter) place
      (funcall setter `(progn
                         (setf (cdr ,x) ,getter)
                         ,x )))))

(defmacro list-pop-link(place)
  "Return the first link in PLACE's list, and set PLACE to its next link.
PLACE must be a generalized variable whose value is a list.
If the value is nil, `list-pop-link' returns nil and does not modify PLACE."
  (declare (debug (gv-place)))
  (list-tools--gv-letplace-let (got setter) place
    `(prog1 ,got
       ,(funcall setter `(cdr ,got)) )))


(gv-define-expander list-pop
  (lambda(do sub-place)
    (macroexp-let2 ignore x `(list-pop-link ,sub-place)
      (gv-get `(car ,x)
              do ))))

;;length

(defun list-length(list)
  "Return the length of LIST.
if LIST ends in a cycle, then this function never returns."
  (declare (pure t)
           (side-effect-free t) )
  (let ((out 0))
    (while list
      (setq out  (1+  out)
            list (cdr list) ))
    out ))

(defun list-safe-length(list)
  "Return the length of a list, but avoid errors or infinite loops.
This function never gives an error.  If LIST is not really a list,
it returns 0.  If LIST is circular, it returns an integer that is at
least the number of distinct elements."
  (declare (pure t)
           (side-effect-free error-free) )
  (if (consp list)
      (let* ((p1 (cdr      list))
             (p2 (cdr-safe p1))
             (num 1) )
        (while (cond
                ((not (consp p2))          ; not a cycle
                 (while (consp p1)
                   (setq p1  (cdr p1)
                         num (1+  num) )))
                ((eq p1 p2)                ; is a cycle
                 (while (not (eq list p1))
                   (setq p1   (cdr p1)
                         list (cdr list)
                         num  (1+  num) )))
                (t                         ; loop
                 (setf p1  (cdr p1)
                       p2  (cdr-safe (cdr p2))
                       num (1+ num) ))))
        num )
    0 ))

;;elt

(defun list-elt(list n)
  "Return the Nth element of LIST."
  (declare (pure t)
           (side-effect-free t) )
  (when (< n 0)
    (signal 'args-out-of-range (list list n)) )
  (if-let (cell (nthcdr n list))
      (car cell)
    (signal 'args-out-of-range (list list n)) ))

(defun list-elt-set(list n val)
  "Set the Nth element of LIST to VAL.
returns VAL."
  (when (< n 0)
    (signal 'args-out-of-range (list list n)) )
  (if-let (spot (nthcdr n list))
      (setf (car spot) val)
    (signal 'args-out-of-range (list list n)) ))

(gv-define-expander list-elt
  (lambda(do sub-place n)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p idx n
        (let ((spot (make-symbol "spot")))
          `(progn
             (when (< ,idx 0)
               (signal 'args-out-of-range (list ,sub-got ,idx)) )
             (if-let ((,spot (nthcdr ,idx ,sub-got)))
                 ,(funcall do `(car ,spot)
                           (lambda(val)
                             `(setf (car ,spot) ,val) ))
               (signal 'args-out-of-range (list ,sub-got ,idx)) )))))))

;;reverse

(defun list-reverse(list)
  "Return a reversed copy of LIST.
See also the function `list-nreverse', which is used more often."
       (declare (pure t)
                (side-effect-free t) )
       (let ((out nil))
         (while list
           (list-push (list-pop list) out) )
         out ))

(defun list-nreverse(list)
  "Reverse order of items in LIST.
This function may destructively modify LIST to produce the value."
  (declare (pure t)
           (side-effect-free t) )
  (let ((out  nil)
        (spot nil) )
    (while list
      (setf spot       list)
      (setf list       (cdr list))
      (setf (cdr spot) out)
      (setf out        spot) )
    out ))

(defmacro list-nreverse-place(place)
  "Reverse the order of items in PLACE.
PLACE should be a nil-terminated list."
  (gv-letplace (getter setter) place
    (funcall setter `(list-nreverse ,getter)) ))

;;TODO: setf?

;;replace

(defmacro list-replace-all-setf(place old-element new-element &optional compare-fn)
  "Replace all instances of OLD-ELEMENT with NEW-ELEMENT in PLACE.
The tests for OLD-ELEMENT are done with COMPARE-FN (defaults to `eq')."
  (gv-letplace (getter setter) place
    (let ((got     (make-symbol "got"))
          (elm     (make-symbol "elm"))
          (new-elm (make-symbol "new-elm"))
          (test-fn (make-symbol "test-fn"))
          (spot    (make-symbol "spot")) )
      `(let ((,got     ,getter)
             (,elm     ,old-element)
             (,new-elm ,new-element)
             (,test-fn (or ,compare-fn #'equal)) )
         (let ((,spot ,got))
           (while ,spot
             (when (funcall ,test-fn (car ,spot) ,elm)
                 (setf (car ,spot) ,new-elm) )
             (list-pop ,spot) ))
         ,(funcall setter got) ))))

;;remove

(defmacro list-remove-first-setf(place element &optional compare-fn)
  "Remove the first instance of ELEMENT in PLACE.
The test for ELEMENT is done with COMPARE-FN (defaults to `eq')."
  (gv-letplace (getter setter) place
    (let ((got     (make-symbol "got"))
          (elm     (make-symbol "elm"))
          (test-fn (make-symbol "test-fn"))
          (spot    (make-symbol "spot")) )
      `(let ((,got     ,getter)
             (,elm     ,element)
             (,test-fn (or ,compare-fn #'equal)) )
         (when ,got
           (if (funcall ,test-fn (car ,got) ,elm)
               ,(funcall setter `(cdr ,got))
             (let ((,spot ,got))
               (while (if (funcall ,test-fn (cadr ,spot) ,elm)
                          (ignore (setf (cdr ,spot) (cddr ,spot)))
                        (setf ,spot (cdr ,spot)) ))
               ,(funcall setter got) )))))))

(defmacro list-remove-all-setf(place element &optional compare-fn)
  "Remove all instances of ELEMENT in PLACE.
The tests for ELEMENT are done with COMPARE-FN (defaults to `eq')."
  (gv-letplace (getter setter) place
    (let ((got     (make-symbol "got"))
          (elm     (make-symbol "elm"))
          (test-fn (make-symbol "test-fn"))
          (spot    (make-symbol "spot")) )
      `(let ((,got     ,getter)
             (,elm     ,element)
             (,test-fn (or ,compare-fn #'equal)) )
         (while (and ,got
                     (funcall ,test-fn (car ,got) ,elm))
           (list-pop ,got) )
         (when-let (,spot ,got)
           (while (if (funcall ,test-fn (cadr ,spot) ,elm)
                      (setf (cdr ,spot) (cddr ,spot))
                    (setf ,spot (cdr ,spot)) )))
         ,(funcall setter got) ))))

(provide 'list/simple)
;; (provide 'list)
;;; simple.el ends here
