;;; loop.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 03, 2022
;; Modified: April 03, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/loop
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'subr-x)
(require 'list-tools)
(require 'list/simple)
(require 'list/build)
(require 'list/predicate)

;;zip

(eval-and-compile
  (defun list-zip(&rest lists)
    ""
    (when lists
      (list-build-while (not (list-contains-p lists nil))
        (let ((lists-i lists))
          (list-build-while lists-i
            (prog1 (list-pop (car lists-i))
              (list-pop lists-i) ))))))

  (defun list-unzip(lists)
    ""
    (apply #'list-zip lists) ))

;;TODO: nzip

;;do

(defmacro list-dolist(spec &rest body)
  "Loop over a list.
Evaluate BODY with VAR bound to each car from LIST, in turn.
Then evaluate RESULT to get return value, default nil.

\(fn (VAR LIST [RESULT]) BODY...)"
  (declare (indent defun))
  (let ((spot (make-symbol "spot")))
    `(let ((,spot ,(nth 1 spec)))
       (while ,spot
         (let ((,(nth 0 spec) (list-pop ,spot)))
           ,@body ))
       ,@(nthcdr 2 spec) )))

(defmacro list-dolist-indexed(spec &rest body)
  "

\(fn (ELMT-VAR INDX-VAR LIST [RESULT]) BODY...)"
  (declare (indent defun))
  (let ((indx (make-symbol "indx")))
    `(let ((,indx -1))
       (list-dolist (,(car spec) ,@(cddr spec))
         (let ((,(cadr spec) (setq ,indx (1+ ,indx))))
           ,@body )))))

(defun list-do(func list)
  ""
  (list-dolist (arg list)
    (funcall func arg) ))

(defun list-do-indexed(func list)
  ""
  (list-dolist-indexed (arg i list)
    (funcall func arg i) ))

(defun list-do*(func &rest lists)
  ""
  (list-dolist (args (apply #'list-zip lists))
    (apply func args) ))

(defun list-do-indexed*(func &rest lists)
  ""
  (list-dolist-indexed (args i (apply #'list-zip lists))
    (apply func i args) ))

(defmacro list-dolists(specs &rest body)
  "

\(fn ((VAR LIST)...) BODY...)"
  (declare (indent defun))
  (let* ((ziped (list-unzip specs))
         (vars  (nth 0 ziped))
         (lists (nth 1 ziped)) )
    `(list-do* (lambda(,@vars) ,@body) ,@lists) ))

(defmacro list-dolists*(specs &rest body)
  "Loop over lists in SPECS.
Evaluate BODY with VAR bound to each car from LIST, in turn.
this process is stacked for each VAR and LIST given, evaluating
BODY with every combination of elements from each LIST. STEP is
evaluated each time the end of LIST is reached. returns the value
of STEP in the first spec.

\(fn ((VAR LIST [STEP])...) BODY...)"
  (declare (indent defun))
  (let* ((spots (list-build-length (i (list-length specs))
                  (make-symbol (format "spot-%s" i)) )))
    `(when-let ,(list-zip spots (list-map #'cadr specs))
       ,@(let ((out body))
           (list-dolists ((spec (list-reverse specs))
                          (spot (list-reverse spots)) )
             (setq out `((list-dolist (,(car spec) ,spot ,@(cddr spec))
                           ,@out ))))
           out ))))

;;map

(defmacro list-maplist(spec &rest body)
  "

\(fn (VAR LIST) BODY...)"
  (declare (indent defun))
  (let ((out  (make-symbol "out"))
        (iter (make-symbol "iter")) )
    `(let* ((,out  (cons nil nil))
            (,iter (list-extend-cons-iter ,out)))
       (list-dolist (,@spec (cdr ,out))
         (funcall ,iter (progn ,@body)) ))))

(defmacro list-maplist-indexed(spec &rest body)
  "

\(fn (ELMT-VAR INDX-VAR LIST) BODY...)"
  (declare (indent defun))
  (let ((indx (make-symbol "indx")))
    `(let ((,indx -1))
       (list-maplist (,(nth 0 spec) ,@(nthcdr 2 spec))
         (let ((,(nth 1 spec) (setq ,indx (1+ ,indx))))
           ,@body )))))

(defun list-map(func list)
  ""
  (list-maplist (arg list)
    (funcall func arg) ))

(defun list-map-indexed(func list)
  ""
  (list-maplist-indexed (x i list)
    (funcall func x i) ))

(defun list-map*(func &rest lists)
  ""
  (list-maplist (args (apply #'list-zip lists))
    (apply func args) ))

(defun list-map-indexed*(func &rest lists)
  ""
  (list-maplist-indexed (args i (apply #'list-zip lists))
    (apply func i args) ))

(defmacro list-maplists(specs &rest body)
  "

\(fn ((VAR LIST)...) BODY...)"
  (declare (indent defun))
  (let* ((ziped (list-unzip specs))
         (vars  (nth 0 ziped))
         (lists (nth 1 ziped)) )
    `(list-map* (lambda(,@vars) ,@body) ,@lists) ))

(defmacro list-maplists*(specs &rest body)
  "

\(fn ((VAR LIST [STEP])...) BODY...)"
  (declare (indent defun))
  (let ((out  (make-symbol "out"))
        (iter (make-symbol "iter")) )
    `(let* ((,out  (cons nil nil))
            (,iter (list-extend-cons-iter ,out)))
       (list-dolists* ,specs
         (funcall ,iter (progn ,@body)) )
       (cdr ,out) )))

;;nmap

(defmacro list-nmaplist(spec &rest body)
  "

\(fn (VAR LIST) BODY...)"
  (declare (indent defun))
  (let ((out  (make-symbol "out"))
        (spot (make-symbol "spot")))
    `(let* ((,out  ,(nth 1 spec))
            (,spot ,out))
       (while ,spot
         (setf (car ,spot) (let ((,(nth 0 spec) (car ,spot)))
                             ,@body ))
         (list-pop ,spot) )
       ,out )))

(defmacro list-nmaplist-indexed(spec &rest body)
  "

\(fn (ELMT-VAR INDX-VAR LIST) BODY...)"
  (declare (indent defun))
  (let ((indx (make-symbol "indx")))
    `(let ((,indx -1))
       (list-nmaplist (,(nth 0 spec) ,@(nthcdr 2 spec))
         (let ((,(nth 1 spec) (setq ,indx (1+ ,indx))))
           ,@body )))))

(defun list-nmap(func list)
  ""
  (list-nmaplist (arg list)
    (funcall func arg) ))

(defun list-nmap-indexed(func list)
  ""
  (list-nmaplist-indexed (arg i list)
    (funcall func arg i) ))

(defun list-nmap*(func &optional nlist &rest lists)
  ""
  (let ((spot nlist))
    (list-dolist (args (apply #'list-zip nlist lists))
      (setf (car spot) (apply func args))
      (list-pop spot) )
    nlist ))

(defun list-nmap-indexed*(func &optional nlist &rest lists)
  ""
  (let ((spot nlist))
    (list-dolist-indexed (args i (apply #'list-zip nlist lists))
      (setf (car spot) (apply func i args))
      (list-pop spot) )
    nlist ))

(defmacro list-nmaplists(specs &rest body)
  "

\(fn ((VAR LIST)...) BODY...)"
  (declare (indent defun))
  (let* ((ziped (list-unzip specs))
         (vars  (nth 0 ziped))
         (lists (nth 1 ziped)) )
    `(list-nmap* (lambda(,@vars) ,@body) ,@lists) ))


(provide 'list/loop)
;; (provide 'list)
;;; loop.el ends here
