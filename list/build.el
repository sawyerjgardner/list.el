;;; build.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 05, 2022
;; Modified: April 05, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/build
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'gv)
(require 'macroexp)
(require 'list-tools)
(require 'list/simple)

;;-last

(defun list--last(list)
  ""
  (while (cdr list)
    (list-pop list) )
  list )

(defun list--njoin(first second)
  ""
  (if first
      (progn
        (setf (cdr (list--last first)) second)
        first )
    second ))


;;extend iter

(defun list-extend-cons-iter(spot)
  "Return a generator for extending SPOT.
returns a function that accepts one argument (VAL). the returned function will
add VAL to the end of SPOT, extending the list. SPOT should be the last cell of
a list. the returned function assumes it's the only thing adding to the end of
the list and the cell it added last time it was called is still the last link in
the list."
  (unless (consp spot)
    (signal 'wrong-type-argument (list 'consp spot)) )
  (lambda(val)
    (setf spot (setf (cdr spot) (cons val nil))) ))

(defun list-extend-cons-iter*(spot)
  "Return a generator for extending SPOT.
returns a function that accepts one argument (VAL). the returned function will
add VAL to the end of the list that SPOT is in. it will try to find the end of
the list, starting from the cell it added last time it was called. SPOT is the
cell it will start with the first time it's called."
  (unless (consp spot)
    (signal 'wrong-type-argument (list 'consp spot)) )
  (lambda(val)
    (let ((next (cons val nil)))
      (list--njoin spot next)
      (setf spot next) )))

(defun list-extend-iter(list)
  "Return a generator for extending LIST.
returns a function that accepts one argument (VAL). the returned function adds
VAL to the end of LIST. it assumes it's the only thing modifying the end of the
list and the cell it added last time it was called is still the last link in the
list."
  (list-extend-cons-iter (list--last list)) )

(defun list-extend-iter*(list)
  "Return a generator for extending SPOT.
returns a function that accepts one argument (VAL). the returned function will
add VAL to the end of LIST, extending it. it will try to find the end of the
list, starting from the cell it added last time it was called."
  (list-extend-cons-iter* (list--last list)) )

;;extend

(defmacro list-extend-while(spec &rest body)
  "While TEST yields non-nil, eval BODY and append the resolt to the end of LIST.
evaluates BODY and add the result to the end of LIST. this assumes it's
the only thing appending to the end of the list and that
the cell it added last time is still the last link.

The order of execution is TEST, BODY, TEST, BODY and so on
until TEST returns nil.

returns LIST or if a new list had to be created, that instead.

\(fn (LIST TEST) BODY...)"
  (declare (indent defun))
  (let ((base (make-symbol "base"))
        (iter (make-symbol "iter")) )
    `(let* ((,base (cons nil ,(car spec)))
            (,iter (list-extend-iter ,base)) )
       (while ,(cadr spec)
         (funcall ,iter (progn ,@body)) )
       (cdr ,base) )))

(defmacro list-extend-while*(spec &rest body)
  "While TEST yields non-nil, eval BODY and append the resolt to the end of LIST.
evaluates BODY and add the result to the end of LIST. it will try to find the
end of the list, starting from the cell it added last time.

The order of execution is TEST, BODY, TEST, BODY and so on
until TEST returns nil.

returns LIST or if a new list had to be created, that instead.

\(fn (LIST TEST) BODY...)"
  (declare (indent defun))
  (let ((base (make-symbol "base"))
        (iter (make-symbol "iter")) )
    `(let* ((,base (cons nil ,(car spec)))
            (,iter (list-extend-iter* ,base)) )
       (while ,(cadr spec)
         (funcall ,iter (progn ,@body)) )
       (cdr ,base) )))


(defmacro list-extend-place-while(spec &rest body)
  "While TEST yields non-nil, eval BODY and append the resolt to the end of PLACE.
evaluates BODY and add the result to the end of PLACE. the changes will not be
assigned to PLACE until the loop exits, but its side effects may be visible.
this assumes it's the only thing appending to the end of the list and that
the cell it added last time is still the last link.

The order of execution is TEST, BODY, TEST, BODY and so on
until TEST returns nil.

returns the value assigned to PLACE.

\(fn (PLACE TEST) BODY...)"
  (declare (indent defun))
  (gv-letplace (getter setter) (car spec)
    (funcall setter `(list-extend-while (,getter ,(cadr spec))
                       ,@body ))))

(defmacro list-extend-place-while*(spec &rest body)
  "While TEST yields non-nil, eval BODY and append the resolt to the end of PLACE.
evaluates BODY and add the result to the end of LIST. the changes will not be
assigned to PLACE until the loop exits, but its side effects may be visible. it
will try to find the end of the list, starting from the cell it added last time.

The order of execution is TEST, BODY, TEST, BODY and so on
until TEST returns nil.

returns the value assigned to PLACE.

\(fn (PLACE TEST) BODY...)"
  (declare (indent defun))
  (gv-letplace (getter setter) (car spec)
    (funcall setter `(list-extend-while* (,getter ,(cadr spec))
                       ,@body ))))

;;build

(defmacro list-build-while(test &rest body)
  "If TEST yields non-nil, eval BODY... and repeat.
The order of execution is thus TEST, BODY, TEST, BODY and so on
until TEST returns nil.

returns a list containing all the results of BODY in order.

\(fn TEST BODY...)"
  (declare (indent defun))
  (let ((base (make-symbol "base"))
        (iter (make-symbol "iter")) )
    `(let* ((,base (cons nil nil))
            (,iter (list-extend-cons-iter ,base)) )
       (while ,test
         (funcall ,iter (progn ,@body)) )
       (cdr ,base) )))

(defmacro list-build-length(spec &rest body)
  "

\(fn (VAR LENGTH) BODY...)"
  (declare (indent defun))
  (let ((indx (make-symbol "indx"))
        (len  (make-symbol "len")) )
    `(let ((,len  ,(cadr spec))
           (,indx -1))
         (list-build-while (> ,len (setf ,indx (1+ ,indx)))
           (let ((,(car spec) ,indx))
             ,@body )))))

;;copy

(defun list-copy(list)
  "Return a shallow copy of LIST."
  (list-build-while list
    (list-pop list) ))

(defun list-copy-to(from to)
  "copy the elements from list FROME to list TO.
if FROM is longer then TO, this throws an error."
  (while from
    (setf (list-pop to) (list-pop from)) ))

;;append

(defun list-append(&rest lists)
  "Concatenate all the arguments into one list.
The result is a list whose elements are the elements of all the arguments.
Each argument should be a nil-terminated list.
The last argument is not copied, just used as the tail of the new list."
  (let* ((out  (cons nil nil))
         (iter (list-extend-cons-iter out))
         (last out) )
    (while (cdr lists)
      (let ((spot (list-pop lists)))
        (while spot
          (setf last (funcall iter (list-pop spot))) )))
    (setf (cdr last) (car lists))
    (cdr out) ))

(defun list-nappend(&rest lists)
  ""
  (if (cdr lists)
      (list--njoin (car lists) (apply #'list-nappend (cdr lists)))
    (car lists) ))

(defun list-concat(&rest lists)
  ""
  (let* ((out  (cons nil nil))
         (iter (list-extend-cons-iter out)) )
    (while lists
      (let ((spot (list-pop lists)))
        (while spot
          (funcall iter (list-pop spot)) )))
    (cdr out) ))

(defmacro list-append-place(place &rest lists)
  ""
  (gv-letplace (getter setter) place
    (funcall setter `(list-append ,getter ,@lists)) ))

(defmacro list-nappend-place(place &rest lists)
  ""
  (gv-letplace (getter setter) place
    (funcall setter `(list-nappend ,getter ,@lists)) ))

(defmacro list-nappend-place*(&rest places)
  "Destructively concatenate all the generalized variable into one list.
assign the tail of each place's value to the value of the place after it.
the value of each place in PLACES is evaluated before assignment. the values
are then appended and assigned in revers order (last to first). the last
argument dose not need to be a generalized variable has it dose not change.

returns the beginning of the list."
  (if (cdr places)
      (gv-letplace (getter setter) (car places)
        (let ((got      (make-symbol "got"))
              (got-next (make-symbol "got-next")))
          `(let ((,got      ,getter)
                 (,got-next (list-nappend-place* ,@(cdr places))))
             ,(funcall setter `(list--njoin ,got ,got-next)) )))
    (car places) ))

(defmacro list-concat-place(place &rest lists)
  ""
  (gv-letplace (getter setter) place
    (funcall setter `(list-concat ,getter ,@lists)) ))



(provide 'list/build)
;; (provide 'list)
;;; build.el ends here
