;;; split.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: May 11, 2022
;; Modified: May 11, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/split
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'list/drop)
(require 'list/last)

;;split when after

;;TODO: find a better name
(defun list-nsplit-when-after(list spot)
  ""
  (list list (when spot
               (prog1 (cdr spot)
                 (setf (cdr spot) nil) ))))

(defun list-split-when-after*(list spot)
  ""
  (if-let ((end (cdr spot)))
      (progn
        (setf (cdr spot) nil)
        (unwind-protect
            (list (list-copy list) end)
          (setf (cdr spot) end) ))
    (list list nil) ))

(defun list-split-when-after**(list spot)
  ""
  (if-let ((end (cdr spot)))
      (progn
        (setf (cdr spot) nil)
        (unwind-protect
            (list (list-copy list) (list-copy end))
          (setf (cdr spot) end) ))
    (list (list-copy list) nil) ))

;;split

(defun list-nsplit(list index)
  ""
  (if (> index 0)
      (list-nsplit-when-after list (list-ndrop list (1- index)))
    (list nil list) ))

(defun list-split*(list index)
  ""
  (if (> index 0)
      (list-split-when-after* list (list-ndrop list (1- index)))
    (list nil list) ))

(defun list-split**(list index)
  ""
  (if (> index 0)
      (list-split-when-after** list (list-ndrop list (1- index)))
    (list nil (when list (list-copy list))) ))

;;split when cdr

(defun list-nsplit-when-cdr(list pred)
  ""
  (if (and list
           (not (funcall pred list)) )
      (list-nsplit-when-after list (list--last-before-cdr1-skip list pred))
    (list nil list) ))

(defun list-split-when-cdr*(list pred)
  ""
  (if (and list
           (not (funcall pred list)) )
      (list-split-when-after* list (list--last-before-cdr1-skip list pred))
    (list nil list) ))

(defun list-split-when-cdr**(list pred)
  ""
  (if (and list
           (not (funcall pred list)) )
      (list-split-when-after** list (list--last-before-cdr1-skip list pred))
    (list nil (when list (list-copy list))) ))

;;split when

(defun list-nsplit-when(list pred)
  ""
  (list-nsplit-when-cdr list (lambda(x) (funcall pred (car x)))) )

(defun list-split-when*(list pred)
  ""
  (list-split-when-cdr* list (lambda(x) (funcall pred (car x)))) )

(defun list-split-when**(list pred)
  ""
  (list-split-when-cdr** list (lambda(x) (funcall pred (car x)))) )

;;split when elt

(defun list-nsplit-when-elt(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-nsplit-when-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(defun list-split-when-elt*(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-split-when-cdr* list (lambda(x) (funcall testfn (car x) elt))) )

(defun list-split-when-elt**(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-split-when-cdr** list (lambda(x) (funcall testfn (car x) elt))) )


;;partition

(defun list-npartition(list index)
  ""
  (when (and list
             (> index 0) )
    (list-build-while list
      (let ((split (list-nsplit list index)))
        (setf list (cadr split))
        (car split) ))))

(defun list-partition*(list index)
  ""
  (when (and list
             (> index 0) )
    (list-build-while list
      (let ((split (list-split* list index)))
        (setf list (cadr split))
        (car split) ))))

(defun list-partition**(list index)
  ""
  (when (and list
             (> index 0) )
    (list-build-while list
      (let ((split (list-split* list index)))
        (setf list (cadr split))
        (if list
            (car split)
          (list-copy (car split)) )))))

;;partition when cdr

(defun list-npartition-when-cdr(list pred)
  ""
  (when list
    (let ((out (when (funcall pred list)
                 (list nil) )))
      (list-extend-while (out list)
        (let ((split (list-nsplit-when-after list (list--last-before-cdr1-skip list pred))))
          (setf list (cadr split))
          (car split) )))))

(defun list-partition-when-cdr*(list pred)
  ""
  (when list
    (let ((out (when (funcall pred list)
                 (list nil) )))
      (list-extend-while (out list)
        (let ((split (list-split-when-after* list (list--last-before-cdr1-skip list pred))))
          (setf list (cadr split))
          (car split) )))))

;;TODO: optimize
(defun list-partition-when-cdr**(list pred)
  ""
  (when list
    (let ((out (when (funcall pred list)
                 (list nil) )))
      (list-extend-while (out list)
        (let ((split (list-split-when-after* list (list--last-before-cdr1-skip list pred))))
          (setf list (cadr split))
          (if list
              (car split)
            (list-copy (car split)) ))))))

;;partition when

(defun list-npartition-when(list pred)
  ""
  (list-npartition-when-cdr list (lambda(x) (funcall pred (car x)))) )

(defun list-partition-when*(list pred)
  ""
  (list-partition-when-cdr* list (lambda(x) (funcall pred (car x)))) )

(defun list-partition-when**(list pred)
  ""
  (list-partition-when-cdr** list (lambda(x) (funcall pred (car x)))) )

;;partition when elt

(defun list-npartition-when-elt(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-npartition-when-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(defun list-partition-when-elt*(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-partition-when-cdr* list (lambda(x) (funcall testfn (car x) elt))) )

(defun list-partition-when-elt**(list elt &optional testfn)
  ""
  (setf testfn (or testfn #'equal))
  (list-partition-when-cdr** list (lambda(x) (funcall testfn (car x) elt))) )


;;popn

(defmacro list--define-popn(name args doc split-fn)
  "
`(fn NAME (PLACE ARGS...) SPLIT-FN)'"
  (declare (indent defun))
  (let ((getter (make-symbol "getter"))
        (setter (make-symbol "setter"))
        (split  (make-symbol "split")))
    `(defmacro ,name ,args
       ,doc
       (gv-letplace (,getter ,setter) ,(car args)
         (macroexp-let2 ignore ,split
                        (list ',split-fn
                              ,getter
                              ,@(remq '&optional (cdr args)) )
           `(progn
              ,(funcall ,setter `(cadr ,,split))
              (car ,,split) ))))))


(list--define-popn list-npopn(place n)
  ""
  list-nsplit )
(list--define-popn list-popn(place n)
  ""
  list-split* )

(list--define-popn list-npop-until(place pred)
  ""
  list-nsplit-when )
(list--define-popn list-pop-until(place pred)
  ""
  list-split-when* )

(list--define-popn list-npop-until-after(place spot)
  ""
  list-nsplit-when-after )
(list--define-popn list-pop-until-after(place spot)
  ""
  list-split-when-after* )

(list--define-popn list-npop-until-cdr(place pred)
  ""
  list-nsplit-when-cdr )
(list--define-popn list-pop-until-cdr(place pred)
  ""
  list-split-when-cdr* )

(list--define-popn list-npop-until-elt(place elt &optional testfn)
  ""
  list-nsplit-when-elt )
(list--define-popn list-pop-until-elt(place elt &optional testfn)
  ""
  list-split-when-elt* )





(provide 'list/split)
;;(provide 'list)
;;; split.el ends here
