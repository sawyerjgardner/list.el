;;; take.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 20, 2022
;; Modified: April 20, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/take
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(require 'list-tools)
(require 'list/build)
(require 'list/drop)
(require 'list/split)
(require 'list/predicate)

;;take region

(defun list-ntake-region(first last)
  "Destructively take the elements in a list from FIRST to LAST.
FIRST and LAST should be cons cells of the same list."
  (when last
    (setf (cdr last) nil) )
  first )

(defun list-take-region(first last)
  "Copy the elements in a list from FIRST to LAST.
FIRST and LAST should be cons cells of the same list."
  (if-let ((end (cdr last)))
      (prog2
          (setf (cdr last) nil)
          (list-copy first)
        (setf (cdr last) end) )
    (list-copy first) ))

(gv-define-expander list-ntake-region
  (lambda(do sub-place spot)
    (gv-letplace (sub-getter sub-setter) sub-place
      (macroexp-let2 ignore split `(list-nsplit-when-after ,sub-getter ,spot)
        (funcall do `(car ,split)
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        ,(funcall sub-setter
                                  `(if (cadr ,split)
                                       (list-append ,v (cadr ,split))
                                     ,v ))
                        ,v ))))))))

;;TODO: optimnise split
(gv-define-expander list-take-region
  (lambda(do sub-place spot)
    (gv-letplace (sub-getter sub-setter) sub-place
      (macroexp-let2 ignore split `(list-split-when-after** ,sub-getter ,spot)
        (funcall do `(car ,split)
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        ,(funcall sub-setter
                                  `(if (cadr ,split)
                                       (list-append ,v (cadr ,split))
                                     (list-copy ,v) ))
                        ,v ))))))))


;;take

(defun list-ntake(list n)
  "Distructivly return the first N elements of LIST."
  (when (and list (> n 0))
    (list-ntake-region list (list-ndrop list (1- n))) ))

(defun list-take(list n)
  "Return a shallow copy of the first N elements of LIST."
  (list-build-while (and list (> n 0))
    (setf n (1- n))
    (list-pop list) ))

(gv-define-expander list-ntake
  (lambda(do sub-place n)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p idx n
        (macroexp-let2* ignore ((start nil)
                                (spot  `(list-ndrop ,sub-got (1- ,idx)))
                                (end   sub-got) )
          `(progn
             (when (or (< ,idx 0)
                       (not (or ,spot
                                (= ,idx 0) )))
               (signal 'args-out-of-range (list ,sub-got ,idx)))
             (when (> ,idx 0)
               (setf ,start ,sub-got
                     ,end   (cdr ,spot)
                     (cdr ,spot) nil ))
             ,(funcall do start
                       (lambda(val)
                         (macroexp-let2 macroexp-copyable-p v val
                           `(progn
                              (setf ,sub-got (if ,end
                                                 (list-append ,v ,end)
                                               ,v ))
                              ,v ))))))))))

(gv-define-expander list-take
  (lambda(do sub-place n)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p idx n
        (macroexp-let2* ignore ((spot  `(list-ndrop ,sub-got (1- ,idx)))
                                (end   `(if (> ,idx 0)
                                            (cdr ,spot)
                                          ,sub-got )))
          `(progn
             (when (or (< ,idx 0)
                       (not (or ,spot
                                (= ,idx 0) )))
               (signal 'args-out-of-range (list ,sub-got ,idx)))
             ,(funcall do `(when (> idx 0)
                             (list--take-at* ,sub-got ,spot) )
                       (lambda(val)
                         (macroexp-let2 macroexp-copyable-p v val
                           `(progn
                              (setf ,sub-got (list-concat ,v ,end))
                              ,v ))))))))))


;;take until cdr

(defun list-ntake-until-cdr(list pred)
  "Destructively return LIST up to the first cell ware PRED returns non-nil.
PRED should be a function that accepts one argument.

PRED is first passed LIST, if PRED returns nil, it will be passed the cdr of
LIST. this is repeated until PRED returns non-nil or the end of LIST is reached.
the return value for this function is a list containing all the elements of LIST
before the first cell that PRED returned non-nil for. LIST may be destructively
modified to produce this value. if the end of LIST is reached and PRED still has
not returned a non-nil value, LIST is returned unmodified.

if this function reaches the last cdr of LIST and its value is not nil, that
value is passed to PRED. if PRED then returns nil, this function will signals an
error.

the value nil is never passed to PRED."
  (when (and list
             (not (funcall pred list)) )
    (list-ntake-region list (list--last-before-cdr1-skip list pred)) ))

(defun list-take-until-cdr(list pred)
  "Return LIST up to the first cell ware PRED returns non-nil.
PRED should be a function that accepts one argument.

PRED is first passed LIST, if PRED returns nil, it will be passed the cdr of
LIST. this is repeated until PRED returns non-nil or the end of LIST is reached.
the return value for this function is a shallow copy of LIST containing all the
elements before the first cell that PRED returned non-nil for. if the end of
LIST is reached and PRED still has not returned a non-nil value, a full copy of
LIST is returned.

if this function reaches the last cdr of LIST and its value is not nil, that
value is passed to PRED. if PRED then returns non-nil, a nil terminated copy of
list is returned, otherwise this function will signals an error.

the value nil is never passed to PRED."
  (list-build-while (and list
                         (not (funcall pred list)) )
    (list-pop list) ))

(gv-define-expander list-ntake-until-cdr
  (lambda(do sub-place pred)
    (gv-letplace (sub-getter sub-setter) sub-place
      (macroexp-let2 ignore split `(list-nsplit-when-cdr ,sub-getter ,pred)
        (funcall do `(car ,split)
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        ,(funcall sub-setter
                                  `(if (cadr ,split)
                                       (list-append ,v (cadr ,split))
                                     ,v ))
                        ,v ))))))))

;;TODO: optimnise split
(gv-define-expander list-take-until-cdr
  (lambda(do sub-place pred)
    (gv-letplace (sub-getter sub-setter) sub-place
      (macroexp-let2 ignore split `(list-split-when-cdr** ,sub-getter ,pred)
        (funcall do `(car ,split)
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        ,(funcall sub-setter
                                  `(if (cadr ,split)
                                       (list-append ,v (cadr ,split))
                                     (list-copy ,v) ))
                        ,v ))))))))

;;drop unti

(defun list-ntake-until(list pred)
  "Take the elements of LIST before the first one which PRED returns non-nil.
returns a list containing the elements of LIST before the first element PRED
returns non-nil for. LIST may be destructively modified to produce this value.
if the end of LIST is reached and PRED still has not returned a non-nil value,
LIST is returned in its antirety.

PRED should be a function that accepts one argument."
  (list-ntake-until-cdr list (lambda(x) (funcall pred (car x)))) )

(defun list-take-until(list pred)
  "Copy the elements of LIST before the first one which PRED returns non-nil.
returns a list containing the elements of LIST before the first element PRED
returns non-nil for. if the end of LIST is reached and PRED still has not
returned a non-nil value, a shallow copy of LIST is returned.

PRED should be a function that accepts one argument."
  (list-take-until-cdr list (lambda(x) (funcall pred (car x)))) )

(gv-define-expander list-ntake-until
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x)))) )
          (gv-get `(list-ntake-until-cdr ,sub-got ,func) do) )))))

(gv-define-expander list-take-until
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x)))) )
          (gv-get `(list-take-until-cdr ,sub-got ,func) do) )))))

;;drop until elt

(defun list-ntake-until-elt(list elt &optional testfn)
  "Take the elements of LIST before the first one that is equal to elt.
returns a list containing the elements of LIST before the first element that is
equal to ELT. LIST may be destructively modified to produce this value. if none
of the elements in LIST is equal to ELT, LIST is returned in its antirety.

Equality is defined by TESTFN; defalts to `equal' if nil."
  (setf testfn (or testfn #'equal))
  (list-ntake-until-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(defun list-take-until-elt(list elt &optional testfn)
  "Copy the elements of LIST before the first one that is equal to ELT.
returns a list containing the elements of LIST before the first element that is
equal to ELT. if none of the elements in LIST is equal to ELT, a shallow copy of
LIST is returned.

Equality is defined by TESTFN; defalts to `equal' if nil."
  (setf testfn (or testfn #'equal))
  (list-take-until-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(gv-define-expander list-ntake-until-elt
  (lambda(do sub-place elt &optional testfn)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* macroexp-copyable-p ((e elt)
                                           (fn `(or ,testfn #'equal)))
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x) ,e))) )
          (gv-get `(list-ntake-until-cdr ,sub-got ,func) do) )))))

(gv-define-expander list-take-until-elt
  (lambda(do sub-place elt &optional testfn)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* macroexp-copyable-p ((e elt)
                                           (fn `(or ,testfn #'equal)))
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x) ,e))) )
          (gv-get `(list-take-until-cdr ,sub-got ,func) do) )))))

(provide 'list/take)
;;(provide 'list)
;;; take.el ends here
