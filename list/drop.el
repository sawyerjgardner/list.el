;;; drop.el --- list drop functions -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 20, 2022
;; Modified: April 20, 2022
;; Version: 0.0.1
;; Keywords: lisp
;; Homepage: https://github.com/sawyer/drop
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; functions for retreaving the latter part of lists
;;
;;; Code:

(require 'list-tools)
(require 'list/build)
(require 'list/predicate)

;;drop until after

;;TODO: find a better name?
;;TODO: find if this function is worth defining
(defun list-ndrop-until-after(_list spot)
  "Return LIST starting at the cdr of SPOT.
This is the same has (`cdr' SPOT)."
  (cdr spot) )

;;TODO: find if this function is worth defining
(defun list-drop-until-after(_list spot)
  "Return a shallow copy of LIST starting at the cdr of SPOT.
this is the same has (`list-copy' (`cdr' SPOT))
See also the function `list-ndrop-until-after', which is used more often."
  (list-copy (cdr spot)) )

(gv-define-expander list-ndrop-until-after
  (lambda(do sub-place spot)
    (list-tools--gv-letplace-simple sub-got sub-place
      (gv-get `(cdr ,spot) do) )))

(gv-define-expander list-drop-until-after
  (lambda(do sub-place spot)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* ignore ((at  spot)
                              (end `(cdr ,at)) )
        (funcall do `(list-copy ,end)
                 (lambda(val)
                   (macroexp-let2 macroexp-copyable-p v val
                     `(progn
                        (setf (cdr ,at) nil)
                        (list-concat-place ,sub-got ,v)
                        (setf (cdr ,at) ,end)
                        ,v ))))))))

;;drop

(defun list-ndrop(list n)
  "Return LIST starting at the N'th element."
  (while (and list
              (> n 0) )
    (setf n (1- n))
    (list-pop list) )
  list )

(defun list-drop(list n)
  "Return a shallow copy of LIST without the first N elements.
See also the function `list-ndrop', which is used more often."
  (list-copy (list-ndrop list n)) )


(gv-define-expander list-ndrop
  (lambda(do sub-place n)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* ignore ((end  sub-got)
                              (len  n)
                              (last nil)
                              (i    len) )
        `(progn
           (when (< ,len 0)
             (signal 'args-out-of-range (list ,sub-got ,len)) )
           (while (and ,end
                       (> ,i 0) )
             (setf ,i    (1- ,i)
                   ,last ,end
                   ,end  (cdr ,end) ))
           (when (> ,i 0)
             (signal 'args-out-of-range (list ,sub-got ,len)) )
           ,(funcall do end
                     (lambda(val)
                       `(if ,last
                            (setf (cdr ,last) ,val)
                          (setf ,sub-got ,val) ))))))))

(gv-define-expander list-drop
  (lambda(do sub-spot n)
    (list-tools--gv-letplace-simple sub-got sub-spot
      (macroexp-let2* ignore ((spot sub-got)
                              (len  n)
                              (out  `(cons nil nil))
                              (iter `(list-extend-cons-iter ,out))
                              (i    len) )
        `(progn
           (when (< ,i 0)
             (signal 'args-out-of-range (list ,sub-got ,len)) )
           (while (and ,spot
                       (> ,i 0) )
             (setf ,i (1- ,i))
             (funcall ,iter (list-pop ,spot)) )
           (when (> ,i 0)
             (signal 'args-out-of-range (list ,sub-got ,len)) )
           ,(funcall do `(list-copy ,spot)
                     (lambda(val)
                       (macroexp-let2 ignore v val
                         `(prog1 ,v
                            (while ,v
                              (funcall ,iter (list-pop ,v)) )
                            (setf ,sub-got (cdr ,out)) )))))))))

;;drop until cdr

(defun list-ndrop-until-cdr(list pred)
  "Return LIST starting at the first cell ware PRED returns non-nil.
PRED should be a function that accepts one argument.

PRED is first passed LIST, if PRED returns nil, it will be passed the cdr of
LIST. this is repeated until PRED returns non-nil or the end of LIST is reached.
the return value for this function is the first call of LIST in whitch PRED
returned non-nil. if the end of the list is reached and PRED still has not
returned a non-nil value, this function returns nil.

if this function reaches the last cdr of LIST and its value is not nil, that
value is passed to PRED. if PRED then returns non-nil, the value passed to it
is returned, otherwise this function signals an error.

the value nil is never passed to PRED."
  (while (and list
              (not (funcall pred list)) )
    (list-pop list) )
  list )

(defun list-drop-until-cdr(list pred)
  "Return a copy of LIST starting at the first cell ware PRED returns non-nil.
PRED should be a function that accepts one argument.

PRED is first passed LIST, if PRED returns nil, it will be passed the cdr of
LIST. this is repeated until PRED returns non-nil or the end of LIST is reached.
the return value for this function is a shallow copy of LIST starting at the
first call in whitch PRED returned non-nil. if the end of the list is reached
and PRED still has not returned a non-nil value, this function returns nil.

the value nil is never passed to PRED.
See also the function `list-ndrop-until-cdr', which is used more often."
  (list-copy (list-ndrop-until-cdr list pred)) )

(gv-define-expander list-ndrop-until-cdr
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (macroexp-let2* ignore ((spot nil)
                                (end  sub-got) )
          `(progn
             (while (and ,end
                         (not (funcall ,fn ,end)) )
               (setf ,spot ,end
                     ,end  (cdr ,end) ))
             ,(funcall do end
                       (lambda(val)
                         `(if ,spot
                              (setf (cdr ,spot) ,val)
                            (setf ,sub-got ,val) )))))))))

(gv-define-expander list-drop-until-cdr
  (lambda(do sub-spot pred)
    (list-tools--gv-letplace-simple sub-got sub-spot
      (macroexp-let2* ignore ((spot sub-got)
                              (out  `(cons nil nil))
                              (iter `(list-extend-cons-iter ,out)) )
        `(progn
           ,(macroexp-let2 macroexp-copyable-p fn pred
              `(while (and ,spot
                           (not (funcall ,fn ,spot)) )
                 (funcall ,iter (list-pop ,spot)) ))
           ,(funcall do `(list-copy ,spot)
                     (lambda(val)
                       (macroexp-let2 ignore v val
                         `(prog1 ,v
                            (while ,v
                              (funcall ,iter (list-pop ,v)) )
                            (setf ,sub-got (cdr ,out)) )))))))))

;;drop until

(defun list-ndrop-until(list pred)
  "Return LIST starting at the first element PRED returns non-nil for.
PRED should be a function that accepts one argument.

PRED is passed each element of LIST sequentualy until it returns a non-nil
value. this function then returns LIST starting at the element PRED returned
non-nil for. if the end of the list is reached and PRED still has not returned
a non-nil value, this function returns nil."
  (list-ndrop-until-cdr list (lambda(x) (funcall pred (car x)))) )

(defun list-drop-until(list pred)
  "Return a copy of LIST starting at the first element PRED returns non-nil for.
PRED should be a function that accepts one argument.

PRED is passed each element of LIST sequentualy until it returns a non-nil
value. this function then returns a shallow copy of LIST starting at the element
PRED returned non-nil for. if the end of the list is reached and PRED still has
not returned a non-nil value, this function returns nil.

See also the function `list-ndrop-until', which is used more often."
  (list-drop-until-cdr list (lambda(x) (funcall pred (car x)))) )

(gv-define-expander list-ndrop-until
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x)))) )
          (gv-get `(list-ndrop-until-cdr ,sub-got ,func) do) )))))

(gv-define-expander list-drop-until
  (lambda(do sub-place pred)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2 macroexp-copyable-p fn pred
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x)))) )
          (gv-get `(list-drop-until-cdr ,sub-got ,func) do) )))))

;;drop until elt

(defun list-ndrop-until-elt(list elt &optional testfn)
  "Return LIST starting at the first element that is equal to ELT.
Equality is defined by TESTFN; defalts to `equal' if nil."
  (setf testfn (or testfn #'equal))
  (list-ndrop-until-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(defun list-drop-until-elt(list elt &optional testfn)
  "Return a copy of LIST starting at the first element that is equal to ELT.
Equality is defined by TESTFN; defalts to `equal' if nil.

See also the function `list-ndrop-until-elt', which is used more often."
  (setf testfn (or testfn #'equal))
  (list-drop-until-cdr list (lambda(x) (funcall testfn (car x) elt))) )

(gv-define-expander list-ndrop-until-elt
  (lambda(do sub-place elt &optional testfn)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* macroexp-copyable-p ((e  elt)
                                           (fn `(or ,testfn #'equal)))
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x) ,e))) )
          (gv-get `(list-ndrop-until-cdr ,sub-got ,func) do) )))))

(gv-define-expander list-drop-until-elt
  (lambda(do sub-place elt &optional testfn)
    (list-tools--gv-letplace-simple sub-got sub-place
      (macroexp-let2* macroexp-copyable-p ((e  elt)
                                           (fn `(or ,testfn #'equal)))
        (let* ((x    (make-symbol "x"))
               (func `(lambda(,x) (funcall ,fn (car ,x) ,e))) )
          (gv-get `(list-drop-until-cdr ,sub-got ,func) do) )))))

(provide 'list/drop)
;;(provide 'list)
;;; drop.el ends here
