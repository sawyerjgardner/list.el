((nil . ((eval . (let ((root (file-name-directory
                              (let ((d (dir-locals-find-file "./")))
                                (if (stringp d) d (car d)) ))))
                   (add-to-list 'load-path root)
                   (add-to-list 'load-path (concat root "/tests")) )))))
