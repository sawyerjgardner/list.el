;;; list-tools.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sawyer Gardner
;;
;; Author: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Maintainer: Sawyer Gardner <sawyerjgardner@gmail.com>
;; Created: April 02, 2022
;; Modified: April 02, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/sawyer/global
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


;;let

(defmacro list-tools--macroexp-let-list(test sym list &rest body)
  "`macroexp-let2*' but works with lists instead of pairs.

TEST is the same has in `macroexp-let2*'.
LIST is a lists of expressions.
SYM is the symbol that LIST will be bound to.

example use:
\(defmacro foo(&rest args)
  \(list-tools--macroexp-let-list nil xs args
     `(something ,@xs)))"
  (declare (indent 3))
  (macroexp-let2* ignore ((in   list)
                          (olet nil) )
    `(let* ((,sym nil))
       (dotimes (x (length ,in))
         (if (funcall #',(or test #'macroexp-const-p) (car ,in))
             (push (pop ,in) ,sym)
           (let ((symbol (make-symbol (format "var-%s" x))))
             (push (list symbol (pop ,in)) ,olet)
             (push symbol ,sym) )))
       (setq ,sym (nreverse ,sym))
       (macroexp-let* (nreverse ,olet)
         ,@body ))))

;;let-pred

(defmacro list-tools--if-let-pred(pred varlist then &rest else)
  ""
  (declare (indent 2) (debug if-let))
  (macroexp-let2 macroexp-copyable-p fn pred
    (if varlist
        `(let ,varlist
           (if ,@(let ((tests (mapcar (lambda(x)
                                     `(funcall ,fn ,(car x)) )
                                   varlist )))
                  (if (= (length tests) 1)
                      tests
                    `((and ,@tests)) ))
               ,then
             ,@else))
      then )))

(defmacro list-tools--while-let(varlist &rest body)
  ""
  (declare (indent 1) (debug if-let))
  `(while (when-let ,varlist
            ,@body
            t )))

;;constant

(defmacro list-tools--if-let-constant(varlist then &rest else)
  ""
  (declare (indent 2) (debug if-let))
  `(list-tools--if-let-pred #'macroexp-const-p ,varlist
     (progn ,@(mapcar (lambda(x)
                        `(setf ,(car x) (eval ,(car x) t)))
                      varlist )
            ,@(macroexp-unprogn then) )
     ,@else))

;;gv

(defmacro list-tools--gv-letplace-let(spec place &rest body)
  "Version  of `gv-letplace' ware GOT is always bound to a symbol.
the value of the symbol bound to GOT will be the value in PLACE at the
time of evaluation. SPEC is the same has in `gv-letplace'.
if the getter for PLACE is a symbol, GOT gets bound to that.
note that this dose not apply changes in VAR to PLACE.
for that see `list-tools--gv-letplace-simple'

\(fn (GOT SETTER) PLACE &rest BODY)"
  (declare (indent defun))
  (let ((getter (make-symbol "getter")))
    `(gv-letplace (,getter ,(nth 1 spec)) ,place
       (macroexp-let2 symbolp ,(nth 0 spec) ,getter
         ,@body ))))

(defmacro list-tools--gv-letplace-simple(var place &rest body)
  "Bind VAR to a symbol that holds the value in PLACE.
simplified version of `gv-letplace'.
if PLACE is a symbol, VAR will be bound to PLACE."
  (declare (indent defun))
  (let ((setter (make-symbol "setter"))
        (out    (make-symbol "out")))
    `(list-tools--gv-letplace-let (,var ,setter) ,place
       (let ((,out ,(macroexp-progn body)))
         (if (symbolp ,place)
             ,out
           `(prog1 ,,out ,(funcall ,setter ,var)) )))))


(defmacro list-tools--gv-define-c-setter(name arglist &rest body)
  "Define a setter method for generalized variable NAME.
like `gv-define-simple-setter' but the first argument passed to NAME (the second
argument in ARGLIST) is expected to be a generalized variable PLACE.

\(fn NAME (VAL PLACE ARGS...) BODY...)"
  (declare (indent defun)
           (debug (&define [&name symbolp "@gv-setter"] sexp def-body)))
  (let ((do            (make-symbol "do"))
        (args          (make-symbol "args"))
        (copyable-args (make-symbol "copyable-args"))
        (sub-place     (make-symbol "sub-place"))
        (val           (nth 0 arglist))
        (got           (nth 1 arglist)) )
    `(gv-define-expander ,name
       (lambda(,do ,sub-place &rest ,args)
         (list-tools--gv-letplace-simple ,got ,sub-place
           (list-tools--macroexp-let-list macroexp-copyable-p ,copyable-args ,args
             (funcall ,do `(,',name ,,got ,@,copyable-args)
                      (lambda(,val)
                        (apply (lambda ,(nthcdr 2 arglist)
                                 ,@body)
                               ,copyable-args )))))))))

;;predicate

(defun list-tools--invert-pred(pred)
  ""
  (lambda(&rest r) (not (apply pred r))) )

(provide 'list-tools)
;;; list-tools.el ends here
